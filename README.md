# Blue Star Sports PlayHoops

## Changing Build File Name

* Edit project name in package.json
* Remove ios and android directories
* Run: ```react-native upgrade```

Note: project name in package.json does not support spaces. You can add spaces in the following way:

### iOS
Open: ```ios/Project_Name.xcodeproj```<br>
Select info.plist under project name subfolder in left tree. Edit value for 'Bundle display name' as required.

### Android
Edit: ```android/app/src/main/res/values/strings.xml```<br>
and set the app_name string.

## NPM Scripts
* To install all dependencies after checking project out from repository: ```npm install```
* Run linter: ```npm run lint```
* Run linter and apply fixes: ```npm run fix```
* Clean project: ```npm run clean```
* Set environment to development: ```npm run dev```
* Set environment to production: ```npm run prod```
* Build android APK: ```npm run android:build```
* Build android APK and install: ```npm run android:install```
* List iOS/XCode build targets: ```npm run ios:targets```
* Open in XCode for iOS build: ```npm run ios:build```
* Run Flow type checked: ```npm run flow```

## Running in simulator
* Launch iOS simulator and run app: ```react-native run-ios```
* Run app on already open Android simulator: ```react-native run-android```

## Environment Configuration
Environment specific values are read from the .env file in the project root. To switch between dev and prod configurations, you can either:

```cp .env.production .env```

Or

```cp .env.development .env```

This will overwrite the environment config with the production or development versions respectively, you can then build the app in the normal way. Alternatively, NPM has scripts set up:

```npm run dev ```

Or

```npm run prod```

Will overwrite the .env file as appropriate, then run the iOS simulator.

## Setting up Icon
* Install app-icon: ```npm install -g app-icon```
* Make sure ImageMagick is installed
	* OSX: ```brew install imagemagick```
	* Linux: ```sudo apt-get install imagemagick```
* Put icon.png in project root, dimensions at least 192x192
* Generate icons with: ```app-icon generate```
* To add labels to icon: ```app-icon label -i icon.png -o output.png --top Top text --bottom Bottom text```

## Setting up Splash Screen
### iOS
* Open the xcodeproj from the iOS folder.
* Drag ```node_modules/react-native-smart-splash-screen/ios/RCTSplashScreen/RCTSplashScreen.xcodeproj``` under main project
* Select main project file, build phases tab
* Drag libRCTSplashScreen.a from Products folder (under ```RCTSplashScreen.xcodeproj```) into Link Binary With Libraries section
* Select Build Settings tab
* Under Search Paths, add ```$(SRCROOT)/../node_modules/react-native-smart-splash-screen/ios/RCTSplashScreen/RCTSplashScreen``` to Header Search Path for both Debug and Release
* Delete LaunchScreen.xib from main project
* Drag contents of ```node_modules/react-native-smart-splash-screen/ios/SplashScreenResource``` into Resources section on left tree view
* Under Project Name in left tree (BlueStarScheuler), open AppDelegate.m
* Add to imports: ```#import "RCTSplashScreen.h" //import interface```
* Add the following line of code, after RCTRootView is defined, and before rootView.backgroundColor:
```[RCTSplashScreen open:rootView withImageNamed:@"splash"]; // activate splashscreen, imagename from LaunchScreen.xib```
* Delete any installed version of app on phone/simulator, and rebuild
* To change splash screen, replace the file splash.png in the resources section of the project
* Open RCTSplashScreen.m, insert following line before view.image:
	```view.contentMode = UIViewContentModeScaleAspectFit;```
* Select LaunchScreen.xib in resources. Select the image, then select the Attributes Inspector
* Under Content Mode, select Aspect Fit

### Android
* Open ```android/settings.gradle```
* Check that the following lines are present:
```
    include ':react-native-smart-splashscreen'
	project(':react-native-smart-splashscreen').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-smart-splash-screen/android')
```
* Open ```android/app/build.gradle```
* Check that the following line is present in dependencies section: ```compile project(':react-native-smart-splashscreen')```
* Add ```drawable/splash.png``` to ```android/app/src/main/res/```
* In MainApplication.java:
```
    ...
    import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;    //import package
    ...
    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RCTSplashScreenPackage()    //register Module
        );
    }
```
* In MainActivity.java:
```
    ...
    import com.reactnativecomponent.splashscreen.RCTSplashScreen;    //import RCTSplashScreen
	import android.os.Bundle;
	//import android.widget.ImageView; // uncomment if opening splash fullscreen
    ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RCTSplashScreen.openSplashScreen(this);   //open splashscreen
        //RCTSplashScreen.openSplashScreen(this, true, ImageView.ScaleType.CENTER_INSIDE);   //open splashscreen fullscreen
        super.onCreate(savedInstanceState);
    }
```
* In ```android/app/src/main/res/values``` add the following to the AppTheme section:
```
	<item name="android:windowIsTranslucent">true</item>
```
    
## Deep Linking

### iOS
* Open the file ```ios/BlueStarScheduler/info.plist```
* Add the following key (```bluestar://``` is the registered URI handler):
```
	<key>CFBundleURLTypes</key>
	<array>
		<dict>
			<key>CFBundleTypeRole</key>
			<string>Editor</string>
			<key>CFBundleURLSchemes</key>
			<array>
				<string>bluestar</string>
			</array>
		</dict>
	</array>
```
* Open file ```ios/BlueStarScheduler/AppDelegate.m```
* Add following import: ```#import "RCTLinkingManager.h"```
* Add handler to AppDelegate:
```
	- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
	{
		return [RCTLinkingManager application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
	}
```
* Select project root, in Build Settings add the following recursive path to header search paths: ```$(SRCROOT)/../node_modules/react-native/Libraries```

### Android
* Open ```android/app/src/main/AndroidManifest.xml```
* Add the following intent filter (where URI handler is ```bluestar://```):
	```
    	<intent-filter android:label="filter_react_native">
            <action android:name="android.intent.action.VIEW" />
            <category android:name="android.intent.category.DEFAULT" />
            <category android:name="android.intent.category.BROWSABLE" />
            <data android:scheme="bluestar" android:host="home" />
        </intent-filter>
	```
        
## Architecture
Aside from React Native, this app uses a few archtectural pieces to connect everything together.

* Redux archticeture with flow control by Redux Sagas - https://github.com/redux-saga/redux-saga
* Switch between live and stubbed API by editing src/config/DebugSettings.js. Stubbed functions output is defined in src/services/StubApi.js, pointing to data in src/stub
* Control which redux stores are saved for persistence by adding/removing Redux reducers by name in the whitelist in src/config/ReduxPersist.js
