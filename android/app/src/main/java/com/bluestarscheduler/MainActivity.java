package com.bluestarscheduler;

import com.facebook.react.ReactActivity;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.i18n.reactnativei18n.ReactNativeI18n;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreen;
import android.widget.ImageView;
import android.os.Bundle;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "BlueStarScheduler";
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    //RCTSplashScreen.openSplashScreen(this);   //open splashscreen
	    RCTSplashScreen.openSplashScreen(this, true, ImageView.ScaleType.CENTER_CROP);   //open splashscreen fullscreen
	    super.onCreate(savedInstanceState);
	}
}
