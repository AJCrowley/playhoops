import { call, put } from 'redux-saga/effects'
import BracketActions from '../redux/BracketRedux'

// attempts to login
export function * bracket(api:Object, action:Object) {
	const { eventId, divisionId } = action
	const result = yield call(api.bracket, { eventId, divisionId })
	if(result.ok) {
		// successful call
		yield put(BracketActions.bracketSuccess(result.data))
	} else {
		// yield error
		yield put(BracketActions.bracketError({ error: result.data }))
	}
}
