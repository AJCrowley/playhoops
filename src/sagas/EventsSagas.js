import { call, put } from 'redux-saga/effects'
import EventsActions from '../redux/EventsRedux'

// attempts to login
export function * events(api:Object, action:Object) {
	const result = yield call(api.events)
	if(result.ok) {
		// successful call
		yield put(EventsActions.eventsSuccess(result.data))
	} else {
		// yield error
		yield put(EventsActions.eventsError({ error: result.data }))
	}
}
