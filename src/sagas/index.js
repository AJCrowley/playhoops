// @flow

import { takeLatest, takeEvery } from 'redux-saga/effects'
import API from '../services/Api'
import StubAPI from '../services/StubApi'
import DebugSettings from '../config/DebugSettings'
import { LoginTypes } from '../redux/LoginRedux'
import { login, resetPassword } from './LoginSagas'
import { SignupTypes } from '../redux/SignupRedux'
import { signup } from './SignupSagas'
import { SearchTypes } from '../redux/SearchRedux'
import { search } from './SearchSagas'
import { FeedItemTypes } from '../redux/FeedItemRedux'
import { follow } from './FeedItemSagas'
import { EventsTypes } from '../redux/EventsRedux'
import { events } from './EventsSagas'
import { TeamsTypes } from '../redux/TeamsRedux'
import { teams } from './TeamsSagas'
import { DivisionListTypes } from '../redux/DivisionListRedux'
import { fetchDivisions } from './DivisionListSagas'
import { EventDetailsTypes } from '../redux/EventDetailsRedux'
import { fetchStandings } from './EventDetailsSagas'
import { BracketTypes } from '../redux/BracketRedux'
import { bracket } from './BracketSagas'
import { fetchGames } from './EventDetailsSagas'

const api = DebugSettings.useStubData ? StubAPI : API.create()

export default function * root():Generator<Array<Object>, void, void> {
	yield [
		takeLatest(LoginTypes.LOGIN_REQUEST, login, api),
		takeLatest(LoginTypes.LOGIN_RESET, resetPassword, api),
		takeLatest(SignupTypes.SIGNUP_REQUEST, signup, api),
		takeLatest(SearchTypes.SEARCH_REQUEST, search, api),
		takeEvery(FeedItemTypes.FOLLOW_REQUEST, follow, api),
		takeLatest(EventsTypes.EVENTS_REQUEST, events, api),
		takeLatest(TeamsTypes.TEAMS_REQUEST, teams, api),
		takeLatest(DivisionListTypes.DIVISION_LIST_REQUEST, fetchDivisions, api),
		takeLatest(BracketTypes.BRACKET_REQUEST, bracket, api),
		takeLatest(EventDetailsTypes.FETCH_STANDINGS_REQUEST, fetchStandings, api),
		takeLatest(EventDetailsTypes.FETCH_GAMES_REQUEST, fetchGames, api),
	]
}
