import { call, put } from 'redux-saga/effects'
import EventDetailsActions from '../redux/EventDetailsRedux'

// attempt fetching standings
export function * fetchStandings(api:Object, action:Object) {
	const { eventId, divisionId } = action
	const result = yield call(api.fetchStandings, { eventId, divisionId })
	if(result.ok) {
		// successful call
		yield put(EventDetailsActions.fetchStandingsSuccess({ result: result.data }))
	} else {
		// yield error
		yield put(EventDetailsActions.fetchStandingsError({ error: result.data }))
	}
}

// attempt fetching schedule results
export function * fetchGames(api:Object, action:Object) {
  const { eventId, divisionId } = action
  const result = yield call(api.fetchGames, { eventId, divisionId })
  if(result.ok) {
    // successful call
    yield put(EventDetailsActions.fetchGamesSuccess(result.data))
  } else {
    // yield error
    yield put(EventDetailsActions.fetchGamesError({ error: result.data }))
  }
}
