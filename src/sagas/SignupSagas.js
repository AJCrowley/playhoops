import { call, put } from 'redux-saga/effects'
import SignupActions from '../redux/SignupRedux'
import { Actions } from 'react-native-router-flux'

// attempts to login
export function * signup(api:Object, action:Object) {
	const { firstName, lastName, email, password } = action
	const result = yield call(api.signup, { firstName, lastName, email, password })
	if(result.ok) {
		// successful call
		yield put(SignupActions.signupSuccess({ result: result.data }))
	} else {
		// yield error
		yield put(SignupActions.signupError({ error: result.data }))
	}
}
