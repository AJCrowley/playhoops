import { call, put } from 'redux-saga/effects'
import TeamsActions from '../redux/TeamsRedux'

// attempts to login
export function * teams(api:Object, action:Object) {
	const result = yield call(api.teams)
	if(result.ok) {
		// successful call
		yield put(TeamsActions.teamsSuccess(result.data))
	} else {
		// yield error
		yield put(TeamsActions.teamsError({ error: result.data }))
	}
}
