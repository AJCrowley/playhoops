import { call, put } from 'redux-saga/effects'
import FeedItemActions from '../redux/FeedItemRedux'

// attempts to login
export function * follow(api:Object, action:Object) {
	const { itemType, id, followStatus } = action
	const result = yield call(api.follow, { itemType, id, followStatus })
	if(result.ok) {
		// successful call
		yield put(FeedItemActions.followSuccess())
	} else {
		// yield error
		yield put(FeedItemActions.followError({ error: result.data }))
	}
}
