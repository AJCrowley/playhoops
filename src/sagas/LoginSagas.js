import { call, put } from 'redux-saga/effects'
import LoginActions from '../redux/LoginRedux'

// attempts to login
export function * login(api:Object, action:Object) {
	const { email, password, remember } = action
	const result = yield call(api.login, { email, password })
	if(result.ok) {
		// successful call
		yield put(LoginActions.loginSuccess({ user: result.data }))
	} else {
		// yield error
		yield put(LoginActions.loginError({ error: result.data }))
	}
}

export function * resetPassword(api:Object, action:Object) {
	const { email } = action
	const result = yield call(api.resetPassword, { email })
	if(result.ok) {
		yield put(LoginActions.loginResetSuccess())
	} else {
		yield put(LoginActions.loginResetError({ error: result.data }))
	}
}
