import { call, put } from 'redux-saga/effects'
import DivisionListActions from '../redux/DivisionListRedux'

// attempts to fetch divisions
export function * fetchDivisions(api, action) {
	const { eventId } = action
	const result = yield call(api.fetchDivisions, { eventId })
	if(result.ok) {
		// successful call
		yield put(DivisionListActions.divisionListSuccess({ results: result.data }))
	} else {
		// yield error
		yield put(DivisionListActions.divisionListError({ error: result.data }))
	}
}