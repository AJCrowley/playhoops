import { call, put } from 'redux-saga/effects'
import SearchActions from '../redux/SearchRedux'

// attempts to login
export function * search(api:Object, action:Object) {
	const { searchText, searchTypes } = action
	const result = yield call(api.search, { searchText, searchTypes })
	if(result.ok) {
		// successful call
		yield put(SearchActions.searchSuccess({ results: result.data }))
	} else {
		// yield error
		yield put(SearchActions.searchError({ error: result.data }))
	}
}
