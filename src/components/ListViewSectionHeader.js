// @flow

import React from 'react'
import {
	Text,
	View
} from 'react-native'
import I18n from 'react-native-i18n'
import { Feeds } from '../styles'

const ListViewSectionHeader = ({ section, category }:Object) => {
	if(category === 'Events' || category === 'Teams') {
		return (
			<View style={Feeds.header}>
				<Text style={Feeds.headerText}>{I18n.t('search.' + category)}</Text>
			</View>
		)
	} else {
		// TODO format category dynamic date to readable date
		// TODO i18n for months?
		return (
			<View style={Feeds.header}>
				<Text style={Feeds.headerText}>{category}</Text>
			</View>
		)
	}
}

export default ListViewSectionHeader
