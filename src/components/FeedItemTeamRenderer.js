// @flow

import React from 'react'
import {
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import I18n from 'react-native-i18n'
import ProgressiveImage from './ProgressiveImage'
import { Feeds } from '../styles'

const FeedItemTeamRenderer = (item:Object) => {
	const nextGame = new Date(item.next).toLocaleString(I18n.currentLocale(), {
		year: 'numeric',
		month: 'short',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	})
	return (
		<View style={Feeds.container}>
			<ProgressiveImage source={{uri: item.image}} style={Feeds.image} />
			<View style={Feeds.details}>
				<Text style={Feeds.text}>Team: {item.name}</Text>
				<Text style={Feeds.text}>Next: {nextGame}</Text>
				<Text style={Feeds.text}>Record: {item.wins} - {item.losses}</Text>
			</View>
		</View>
	)
}

export default FeedItemTeamRenderer
