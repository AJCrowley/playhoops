import React, { Component } from 'react'
import {
	Image,
	Animated,
	View
} from 'react-native'

export default class ProgressiveImage extends Component {
	static propTypes = {
		source: React.PropTypes.object.isRequired,
		backgroundColor: React.PropTypes.string.isRequired,
		resizeMode: React.PropTypes.string.isRequired,
		style: React.PropTypes.any,
		animationTime: React.PropTypes.number.isRequired
	}

	static defaultProps = {
		backgroundColor: '#ccc',
		resizeMode: 'cover',
		animationTime: 500,
		style: {}
	}

	state:Object = {
		opacity: null,
		thumbnailOpacity: null,
		imageLoaded: false
	}

	constructor(props:Object) {
		super(props)
		this.state = {
			opacity: new Animated.Value(props.thumbnail ? 1 : 0),
			thumbnailOpacity: props.thumbnail ? new Animated.Value(0) : null,
			imageLoaded: false
		}
	}

	onThumbnailLoad = () => {
		if(!this.state.imageLoaded) {
			Animated.timing(this.state.thumbnailOpacity, {
				toValue: 1,
				duration: this.props.animationTime
			}).start()
		}
	}

	onLoad = () => {
		this.setState({
			imageLoaded: true
		})
		if(this.props.thumbnail) {
			Animated.timing(this.state.thumbnailOpacity, {
				toValue: 0,
				duration: this.props.animationTime
			}).start()
		} else {
			Animated.timing(this.state.opacity, {
				toValue: 1,
				duration: this.props.animationTime
			}).start()
		}
	}

	render() {
		const { thumbnailOpacity, opacity } = this.state
		const { children, thumbnail, width, height, style, backgroundColor, resizeMode, source } = this.props
		return (
			<View backgroundColor={backgroundColor} width={width} height={height}>
				<Animated.Image
					style={[style, { position: thumbnail ? 'absolute' : 'relative', opacity: opacity }]}
					resizeMode={resizeMode}
					source={source}
					onLoad={this.onLoad}>
					{ !thumbnail &&
						children
					}
				</Animated.Image>
				{ thumbnail &&
					<Animated.Image 
						resizeMode={resizeMode}
						style={[style, { opacity: thumbnailOpacity }]}
						source={thumbnail}
						onLoad={this.onThumbnailLoad} />
				}
				{ thumbnail &&
					<View style={{position: 'absolute'}}>
						{ children }
					</View>
				}
			</View>
		)
	}
}
