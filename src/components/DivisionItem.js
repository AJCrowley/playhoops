// @flow

import React from 'react'
import {
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import {
	ComponentStyle,
	EventDetails
} from '../styles'

const DivisionItem = (props:Object) => {
	const { name, divisionId, onDivisionPress, truncLength }:Object = props
	return (
		<TouchableOpacity onPress={onDivisionPress} >
			<View style={[EventDetails.listItemContainer]}>
				<Text style={[EventDetails.text]}>
					{(name.length < truncLength) ? name : name.slice(0,truncLength) + '...' }
				</Text>
				<Icon style={[ComponentStyle.rightFacingChevron]} name="ios-arrow-forward" />
			</View>
		</TouchableOpacity>
	)
}

DivisionItem.defaultProps = { truncLength: 80 }

export default DivisionItem
