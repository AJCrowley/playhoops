// @flow

import React from 'react'
import {
	Linking,
	TouchableOpacity
} from 'react-native'

/*
	Component to handle link launch
	url can be of any registered handler type, e.g.:
		tel://8002825739
		http://www.google.com
		geo://44.6355333,-63.5943662
	If handler is not registered, calls function passed as onError prop.
	If onError not defined, defaults to console output
*/

const launchLink = (link:string, onError:Function) => {
	// can we open this link type?
	Linking.canOpenURL(link).then(supported => {
		// nope, just put an error on console for now
		if (!supported) {
			onError('Unable to open link: ' + link)
		} else {
			// yes! attempt to open
			return Linking.openURL(link)
		}
		// just in case something went wrong
	}).catch(err => onError(err))
}

const Link = (props:Object) => {
	const { url, children, onError }:Object = props
	return (
		<TouchableOpacity onPress={() => launchLink(url, onError)}>
			{children}
		</TouchableOpacity>
	)
}

Link.defaultProps = {
	onError: (err:string) => console.tron.display({
		name: 'Link Error',
		preview: err,
		value: err
	})
}

export default Link
