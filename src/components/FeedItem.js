// @flow

import React from 'react'
import {
	Image,
	Text,
	View
} from 'react-native'
import FeedItemTeamRenderer from './FeedItemTeamRenderer'
import FeedItemEventRenderer from './FeedItemEventRenderer'
import EventScheduleItem from './EventScheduleItem'

const FeedItem = (item:Object) => {
	const renderItem = (item:Object) => {
		// determine item type, return appropriate renderer
		switch(item.type) {
			case 'teams':
				return <FeedItemTeamRenderer {...item} />
			case 'events':
				return <FeedItemEventRenderer {...item} />
			//in the event of dynamic section headers
			default:
				return <EventScheduleItem {...item} />
		}
	}

	return (
		<View>
			{ renderItem(item) }
		</View>
	)
}

export default FeedItem
