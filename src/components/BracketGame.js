// @flow

import React from 'react'
import {
	Text,
	View
} from 'react-native'
import I18n from 'react-native-i18n'
import Link from './Link'
import ProgressiveImage from './ProgressiveImage'
import {
	Brackets,
	ComponentStyle,
	EventDetails,
	LayoutStyle,
	Metrics
} from '../styles'

const BracketGame = (props:Object) => {
	const { column, truncLength, venue, home_image, homescore, visitor_image, visitorscore, game_date }:Object = props
	let { home_name, visitor_name } = props
	const homeImage = home_image
		? <ProgressiveImage source={{uri: home_image}} backgroundColor='#fff' animationTime={500} style={Brackets.gameImage} resizeMode='contain' />
		: <View style={Brackets.gameImage} />
	const visitorImage = visitor_image
		? <ProgressiveImage source={{uri: visitor_image}} backgroundColor='#fff' animationTime={500} style={Brackets.gameImage} resizeMode='contain' />
		: <View style={Brackets.gameImage} />
	const date = new Date(game_date).toLocaleString(I18n.currentLocale(), {
		year: 'numeric',
		month: 'short',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	})
	// truncate team names if necessary
	home_name = home_name ? (home_name.length < truncLength) ? home_name : home_name.slice(0, truncLength) + '...' : I18n.t('eventBracketScreen.tba')
	visitor_name = visitor_name ? (visitor_name.length < truncLength) ? visitor_name : visitor_name.slice(0, truncLength) + '...' : I18n.t('eventBracketScreen.tba')
	return (
		/* Use marginVertical below to set spacing between game containers as tree progresses */
		<View style={[Brackets.gameContainer, {marginVertical: Metrics.baseMargin + (column * 86)}]}>
			<Text>{venue.name}</Text>
			<View style={Brackets.row}>
				{homeImage}
				<Text style={[Brackets.team, Brackets.homeTeam]}>{home_name}</Text>
				<Text style={[Brackets.score, Brackets.homeScore]}>{homescore}</Text>
			</View>
			<View style={Brackets.row}>
				{visitorImage}
				<Text style={[Brackets.team, Brackets.visitorTeam]}>{visitor_name}</Text>
				<Text style={[Brackets.score, Brackets.visitorScore]}>{visitorscore}</Text>
			</View>
			<Text>{date}</Text>
		</View>
	)
}

BracketGame.defaultProps = {
	truncLength: 42
}

export default BracketGame
