// @flow

import React, { Component } from 'react'
import {
	Animated,
	Easing,
	ScrollView,
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import NativeMethodsMixin from 'NativeMethodsMixin'
import Globals from '../config/GlobalSettings'
import BracketGame from '../components/BracketGame'
import BracketControlBar from '../components/BracketControlBar'
import {
	Brackets,
	LayoutStyle,
	Metrics
} from '../styles'

class BracketsDisplay extends Component {
	state:Object = {}
	
	constructor(props:Object) {
		super(props)
		this.state = {
			minZoom: (1 / props.tree.leaves.length),
			zoomedIn: true,
			bracketWidth: 0,
			bracketHeight: 0,
			bracketOpacity: new Animated.Value(0),
			offsetX: 0
		}
	}

	toggleView = () => {
		// get full dimensions of bracket and zoom state
		const { bracketWidth, bracketHeight, zoomedIn } = this.state
		// are we zoomed in or out?
		if(zoomedIn) {
			// animate zoom out to full view
			this.refs.horizView.scrollResponderZoomTo({
				x: 0,
				y: 0,
				width: bracketWidth,
				height: bracketHeight,
				animated: true,
			})
		} else {
			// animate zoom out to zoomed view
			this.refs.horizView.scrollResponderZoomTo({
				x: 0,
				y: 0,
				width: Metrics.screenWidth,
				height: bracketHeight,
				animated: true,
			})
		}
		this.setState({
			zoomedIn: !zoomedIn
		})
	}

	layoutComplete = (event:Object) => {
		// layout done, store bracket dimensions
		this.setState({
			bracketWidth: event.nativeEvent.layout.width,
			bracketHeight: event.nativeEvent.layout.height
		})
	}

	bracketsScrolled = (event:Object) => {
		// update zoom state if user has scaled view
		this.setState({
			zoomedIn: event.nativeEvent.zoomScale == 1 ? true : false
		})
	}

	snapToColumn = (offset:Object) => {
		this.refs.horizView.scrollTo({
			x: Math.round(offset.x / Metrics.screenWidth) * Metrics.screenWidth,
			easing: Easing.out
		})
	}

	onScrollEnd = (event:Object) => {
		this.setState({
			offsetX: event.nativeEvent.contentOffset.x
		})
	}

	moveColumn = (direction:integer) => {
		let move
		// check we're not already at the end
		if(!(this.state.offsetX == 0 && direction < 0) && !(this.state.offsetX >= (this.state.bracketWidth - Metrics.screenWidth) && direction > 0)) {
			// already locked on a column?
			if(this.state.offsetX % Metrics.screenWidth == 0) {
				move = this.state.offsetX + (Metrics.screenWidth * direction)
			} else {
				// we're between columns
				if(direction > 0) {
					// moving right
					move = Math.ceil(this.state.offsetX / Metrics.screenWidth) * Metrics.screenWidth
				} else {
					// moving left
					move = Math.floor(this.state.offsetX / Metrics.screenWidth) * Metrics.screenWidth
				}
			}
			this.refs.horizView.scrollTo({
				x: move,
				easing: Easing.out
			})
		}
	}

	render() {
		// render bracket columns
		const { title, leaves } = this.props.tree
		const { minZoom, bracketOpacity, zoomedIn } = this.state
		Animated.timing(
			bracketOpacity,
			{
				toValue: 1,
				duration: Globals.animation.baseDuration,
				easing: Easing.inout
			}
		).start()
		return (
			<View>
				<Text style={Brackets.title}>{title}</Text>
				<ScrollView ref="horizView"
					onScroll={this.bracketsScrolled}
					onMomentumScrollEnd={this.onScrollEnd}
          			onScrollEndDrag={this.onScrollEnd}
					horizontal={true}
					style={Brackets.horizContainer}
					bouncesZoom={true}
					removeClippedSubviews={true}
					maximumZoomScale={1}
					minimumZoomScale={minZoom}>
					<Animated.ScrollView ref="mainView" style={[{opacity: bracketOpacity}, Brackets.vertContainer]} onLayout={(event) => this.layoutComplete(event)}>
						<View style={Brackets.roundTitleView}>
							{leaves.map((cv, index) => <Text key={index} style={Brackets.roundTitle}>{cv.title}</Text>)}
						</View>
						<View style={Brackets.bracketView}>
							{leaves.map((curColumn, colIndex) => (
								<View style={Brackets.column} key={colIndex}>
									{curColumn.games.map((curGame, gameIndex) => <BracketGame key={gameIndex} column={colIndex} {...curGame} />)}
								</View>
							))}
						</View>
					</Animated.ScrollView>
				</ScrollView>
				<BracketControlBar
					zoomedIn={zoomedIn}
					toggleZoom={this.toggleView}
					onLeft={() => this.moveColumn(-1)}
					onRight={() => this.moveColumn(1)} />
			</View>
		)
	}
}

export default BracketsDisplay
