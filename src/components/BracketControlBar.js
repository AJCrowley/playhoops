// @flow

import React from 'react'
import {
	TouchableOpacity,
	View
} from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import MatIcon from 'react-native-vector-icons/MaterialIcons'
import {
	Brackets,
	LayoutStyle
} from '../styles'

const BracketControlBar = (props:Object) => {
	const zoomedIn = props.zoomedIn ? 'zoom-out' : 'zoom-in'
	const { onLeft, toggleZoom, onRight } = props
	return (
		<View style={Brackets.controlBarContainer}>
			<View style={Brackets.controlBar}>
				<TouchableOpacity onPress={onLeft}>
					<IonIcon name="ios-arrow-dropleft" style={Brackets.controlBarIcon} />
				</TouchableOpacity>
				<TouchableOpacity onPress={toggleZoom}>
					<MatIcon name={zoomedIn} style={Brackets.controlBarIcon} />
				</TouchableOpacity>
				<TouchableOpacity onPress={onRight}>
					<IonIcon name="ios-arrow-dropright" style={Brackets.controlBarIcon} />
				</TouchableOpacity>
			</View>
		</View>
	)
}

export default BracketControlBar
