// @flow

import React, { Component } from 'react'
import {
	TouchableOpacity,
	View
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { Feeds } from '../styles'

export default class FeedItemTeamRenderer extends Component {
	state:Object = {
		expandIcon: null,
		followIcon: null
	}

	constructor(props:Object) {
		super(props)
		this.state = {
			expandIcon: props.expanded ? 'ios-arrow-dropup' : 'ios-arrow-dropdown',
			followIcon: props.followed ? 'ios-remove-circle' : 'ios-add-circle-outline'
		}
	}

	componentWillReceiveProps(newProps:Object) {
		this.setState({
			expandIcon: newProps.expanded ? 'ios-arrow-dropup' : 'ios-arrow-dropdown',
			followIcon: newProps.followed ? 'ios-remove-circle' : 'ios-add-circle-outline'
		})
	}
	
	render() {
		const { toggle, follow }:Object = this.props
		const { expandIcon, followIcon }:Object = this.state
		return (
			<View style={Feeds.controlContainer}>
				<View style={Feeds.controls}>
					<TouchableOpacity onPress={toggle}>
						<Icon name={expandIcon} style={Feeds.controlIcons} />
					</TouchableOpacity>
					<TouchableOpacity onPress={follow}>
						<Icon name={followIcon} style={Feeds.controlIcons} />
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}
