// @flow

import React from 'react'
import {
	Animated,
	Dimensions,
	Image,
	Platform,
	StyleSheet,
	View
} from 'react-native'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import { NavBar } from 'react-native-router-flux'
import ProgressiveImage from './ProgressiveImage'
import { Colors } from '../styles'

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#EFEFF2',
    paddingTop: 0,
    top: 0,
    ...Platform.select({
      ios: {
        height: 64,
      },
      android: {
        height: 54,
      },
    }),
    right: 0,
    left: 0,
    borderBottomWidth: 0.5,
    borderBottomColor: '#828287',
    position: 'absolute',
  },
  headerImage: {
	flexGrow: 1,
	...Platform.select({
      ios: {
        height: 64,
      },
      android: {
        height: 54,
      },
    }),
	width: Dimensions.get('window').width
  },
  rightButton: {
    height: 37,
    position: 'absolute',
    ...Platform.select({
      ios: {
        top: 22,
      },
      android: {
        top: 10,
      },
    }),
    right: 2,
    padding: 8,
  },
  leftButton: {
    height: 37,
    position: 'absolute',
    ...Platform.select({
      ios: {
        top: 20,
      },
      android: {
        top: 8,
      },
    }),
    left: 2,
    padding: 8,
  }
});

class CustomNavBar extends NavBar {
	//headerImage:Object = null
	//headerImageChanged:boolean = false
	state:Object = {
		headerImage: null,
		headerImageChanged: false
	}

	constructor(props:Object) {
		super(props)
		this.state = {
			headerImage: null,
			headerImageChanged: false
		}
	}

	componentWillReceiveProps(newProps:Object) {
		const headerImage = newProps.headerImage ? {
			uri: newProps.headerImage,
			width: Dimensions.get('window').width,
			height: (Platform.OS === 'ios') ? 64 : 54,
			resizeMode: 'cover'
		} : null
		this.setState({ headerImage })
	}

	render() {
		let state = this.props.navigationState
		let selected = state.children[state.index]
		while ({}.hasOwnProperty.call(selected, 'children')) {
			state = selected
			selected = selected.children[selected.index]
		}
		const navProps = { ...this.props, ...selected }
		const wrapByStyle = (component, wrapStyle) => {
			if (!component) { return null }
			return props => <View style={wrapStyle}>{component(props)}</View>
		}

		const { headerImage } = this.state

		const leftButtonStyle = [styles.leftButton, { alignItems: 'flex-start' }]
		const rightButtonStyle = [styles.rightButton, { alignItems: 'flex-end' }]

		const renderLeftButton = wrapByStyle(selected.renderLeftButton, leftButtonStyle) ||
			wrapByStyle(selected.component.renderLeftButton, leftButtonStyle) ||
			this.renderLeftButton;
		const renderRightButton = wrapByStyle(selected.renderRightButton, rightButtonStyle) ||
			wrapByStyle(selected.component.renderRightButton, rightButtonStyle) ||
			this.renderRightButton;
		const renderBackButton = wrapByStyle(selected.renderBackButton, leftButtonStyle) ||
			wrapByStyle(selected.component.renderBackButton, leftButtonStyle) ||
			this.renderBackButton;
		const renderTitle = selected.renderTitle ||
			selected.component.renderTitle ||
			this.props.renderTitle;
		const navigationBarBackgroundImage = headerImage ||
			this.props.navigationBarBackgroundImage ||
			state.navigationBarBackgroundImage;
		const contents = (
			<View>
				{renderTitle ? renderTitle(navProps) : state.children.map(this.renderTitle, this)}
				{renderBackButton(navProps) || renderLeftButton(navProps)}
				{renderRightButton(navProps)}
			</View>
		)
		
		const renderResult = (
			<Animated.View
				style={[
					styles.header,
					this.props.navigationBarStyle,
					state.navigationBarStyle,
					selected.navigationBarStyle
				]}>
				{navigationBarBackgroundImage ? (
					<ProgressiveImage source={navigationBarBackgroundImage} backgroundColor='#fff' animationTime={1000} style={styles.headerImage}>
						<LinearGradient colors={[Colors.windowTint, Colors.white]} style={styles.headerImage}>
							{contents}
						</LinearGradient>
					</ProgressiveImage>
				) : contents}
			</Animated.View>
		)

		return renderResult
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		headerImage: state.navigator.headerImage
	}
}

export default connect(mapStateToProps, null)(CustomNavBar)
