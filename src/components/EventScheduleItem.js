// @flow

import React from 'react'
import {
	Image,
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import I18n from 'react-native-i18n'
import { Actions } from 'react-native-router-flux'
import { Feeds } from '../styles'

const EventScheduleItem = (item:Object) => {
	/**
	 * TODO: status from api yaml has yet to be explained, may need to check date instead
	 * ASSUME: Status 1 means in progress/finished
	 * If status 0 only means not in progress then this will require checking of dates to determine if the game
	 * is finished or yet to be played
	 * TODO: Library for formatting dates?
	 * TODO: Embedded data for facility?
	 */
	const { navBarTitle } = item.injectedProps
	return (
		<TouchableOpacity style={Feeds.container} onPress={() => Actions.gameDetails({gameId:item.game_id, navBarTitle})}>
			<View style={Feeds.details}>
				{(item.status === 0) ? upcomingRender(item) : resultsRender(item)}
			</View>
		</TouchableOpacity>
	)
}

const upcomingRender = (item:Object) => {
	return (
		<View>
			<View>
				<Text style={Feeds.text}>{item.teams[0].title}</Text>
				<Text style={Feeds.text}>{item.teams[1].title}</Text>
				<Text style={Feeds.text}>{`Facility id: ${item.facility.id}`}</Text>
			</View>
			<View>
				<Text style={Feeds.text}>{item.start_time}</Text>
				<Text style={Feeds.text}>{`${I18n.t('eventScheduleScreen.court')} ${item.court}`}</Text>
			</View>
		</View>
	)
}

const resultsRender = (item:Object) => {
	return (
		<View>
			<View>
				<Text style={Feeds.text}>{`${item.teams[0].title} --- ${item.home_score}`}</Text>
				<Text style={Feeds.text}>{`${item.teams[1].title} --- ${item.away_score}`}</Text>
			</View>
			<View>
				<Text>{'final/inProgress'}</Text>
			</View>
		</View>
	)
}

export default EventScheduleItem
