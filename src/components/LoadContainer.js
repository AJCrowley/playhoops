// @flow

import React from 'react'
import {
	ActivityIndicator
} from 'react-native'

const LoadContainer = (props:Object) => {
	return (
		props.fetching
		? <ActivityIndicator animating={true} style={props.style} color={props.color} size={props.size} />
		: props.children
	)
}

export default LoadContainer
