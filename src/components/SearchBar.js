// @flow

import React, { Component } from 'react'
import {
	TextInput,
	View
} from 'react-native'
import Icon  from 'react-native-vector-icons/Ionicons'
import I18n from 'react-native-i18n'
import { NavBarStyle } from '../styles'

export default class SearchBar extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		/*
		when SearchBar appears, focus search input automatically if autoFocus=true
		Note: if we wish this to autoFocus from other pages but the searchBar is already mounted
		then componentDidMount will not fire a second time, we would need to check for updated
		route navigation and focus accordingly
		*/
		if(this.props.autoFocus){
			this.refs['searchInput'].focus()
		}
	}

	render() {
		const props = this.props
		const onFocus = props.onFocus ? () => props.onFocus() : null
		return (
			<View style={NavBarStyle.searchContainer}>
				<Icon name="ios-search" style={NavBarStyle.button}/>
				<TextInput style={NavBarStyle.searchBar}
					ref="searchInput"
					onChangeText={(text) => props.onChangeText(text)}
					onFocus={onFocus}
					placeholder={props.placeholder}
				/>
			</View>
		)
	}
}
