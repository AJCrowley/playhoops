// @flow

import React, { Component } from 'react'
import {
	Text,
	TouchableHighlight,
	View
} from 'react-native'
import ButtonAbstract from './ButtonAbstract'
import {
	Colors,
	ComponentStyle,
	LayoutStyle,
	Metrics
} from '../styles'

export default class Button extends ButtonAbstract {
	render() {
		const props = this.props
		return (
			<TouchableHighlight style={ComponentStyle.buttonWrapper}
				underlayColor={Colors.transparent}
				onHideUnderlay={() => {this.setStatus(false)}}
				onShowUnderlay={() => {this.setStatus(true)}}
				onPress={props.clickHandler}>
				<View style={this.state.pressStatus ? [props.buttonStyle, props.buttonDownStyle] : props.buttonStyle}>
					<Text style={this.state.pressStatus ? [props.buttonTextStyle, props.buttonDownTextStyle] : props.buttonTextStyle}>{props.text}</Text>
				</View>
			</TouchableHighlight>
		)
	}
}
