// @flow

import React, { Component } from 'react'
import {
	Animated,
	Linking
} from 'react-native'
import { Actions } from 'react-native-router-flux'

const console:Object = {}

export default class FeedItemBaseRenderer extends Component {
	state:Object = {
		expanded: false,
		followed: null,
		expandAnim: null
	}

	constructor(props:Object) {
		super(props)
		// set default state
		this.state = {
			expanded: false,
			followed: props.followed,
			expandAnim: new Animated.Value(0)
		}
	}

	itemPressed = (name:string) => {
		const { id, type, bannerImage }:Object = this.props
		switch(type) {
			case 'events':
				Actions.divisionList({ id: id, navBarTitle: name, bannerImage: bannerImage })
				break
			case 'teams':
				console.tron.log(`NavigationActions.teamdetail({id=${this.props.id}})`)
				break
		}
	}

	toggleExpanded = () => {
		// toggle expanded state of item
		this.setState({
			expanded: !this.state.expanded
		})
	}

	toggleFollow = () => {
		// get object type and ID
		const { type, id } = this.props
		// set followstate
		const followState = !this.state.followed
		// call follow on server
		this.props.followRequest(type, id, followState)
		// update state
		this.setState({
			followed: followState
		})
	}
}
