// @flow

import React from 'react'
import {
	Text,
	TouchableHighlight,
	View
} from 'react-native'
import ButtonAbstract from './ButtonAbstract'
import LoadContainer from './LoadContainer'
import {
	Colors,
	ComponentStyle,
	LayoutStyle,
	Metrics
} from '../styles'

export default class ActivityButton extends ButtonAbstract {
	constructor(props:Object) {
		super(props)
		this.state = {
			pressStatus: false,
			animating: props.animating
		}
	}

	componentWillReceiveProps(newProps:Object) {
		this.setState({
			animating: newProps.animating
		})
	}
	
	render() {
		const props = this.props
		const { animating } = this.state
		return (
			<TouchableHighlight style={ComponentStyle.buttonWrapper}
				underlayColor={Colors.transparent}
				onHideUnderlay={() => {this.setStatus(false)}}
				onShowUnderlay={() => {this.setStatus(true)}}
				onPress={props.clickHandler}>
				<View style={this.state.pressStatus || animating ? [props.buttonStyle, props.buttonDownStyle] : props.buttonStyle}>
					<LoadContainer fetching={animating} color={Colors.white}>
						<Text style={this.state.pressStatus ? [props.buttonTextStyle, props.buttonDownTextStyle] : props.buttonTextStyle}>{props.text}</Text>
					</LoadContainer>
				</View>
			</TouchableHighlight>
		)
	}
}
