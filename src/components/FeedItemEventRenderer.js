// @flow

import React from 'react'
import {
	Animated,
	Easing,
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import I18n from 'react-native-i18n'
import FeedItemBaseRenderer from './FeedItemBaseRenderer'
import Link from './Link'
import ProgressiveImage from './ProgressiveImage'
import FeedItemControls from './FeedItemControls'
import FeedItemActions from '../redux/FeedItemRedux'
import Globals from '../config/GlobalSettings.js'
import {
	Feeds,
	ComponentStyle
} from '../styles'

class FeedItemEventRenderer extends FeedItemBaseRenderer {
	state:Object
	
	constructor(props:Object) {
		super(props)
	}

	render() {
		// build formatted date
		const eventDate = new Date(this.props.date).toLocaleString(I18n.currentLocale(), {
			year: 'numeric',
			month: 'short',
			day: 'numeric',
			hour: 'numeric',
			minute: 'numeric'
		})
		// get item fields
		const { image, name, location, ages, eventType, district, license, contact } = this.props
		// get item control states
		const { expanded, followed } = this.state
		// setup animation
		const animHeight = this.state.expandAnim.interpolate({
			inputRange: [0, 1],
			outputRange: [0, 150]
		})
		Animated.timing(
			this.state.expandAnim,
			{
				toValue: expanded ? 1 : 0,
				duration: Globals.animation.baseDuration,
				easing: Easing.inout
			}
		).start()
		return (
			<TouchableOpacity onPress={() => {this.itemPressed(name)}}>
				<View style={Feeds.container}>
					<ProgressiveImage source={{uri: image}} style={Feeds.image} />
					<View style={Feeds.details}>
						<Text style={Feeds.text}>{name}</Text>
						<Text style={Feeds.text}>{eventDate}</Text>
						<Text style={Feeds.text}>{location}</Text>
					</View>
					<FeedItemControls toggle={this.toggleExpanded} follow={this.toggleFollow} followed={followed} expanded={expanded} />
					<Icon style={[ComponentStyle.rightFacingChevron]} name="ios-arrow-forward" />
				</View>
				<Animated.View style={[Feeds.extendedDetails, {height: animHeight, opacity: this.state.expandAnim }]}>
					<Text style={Feeds.text}>{ages}</Text>
					<Text style={Feeds.text}>{eventType}</Text>
					<Text style={Feeds.text}>{district}</Text>
					<Text style={Feeds.text}>{license}</Text>
					<Link url='https://www.google.ca/maps/search/44.6355333,-63.5943662?hl=en&source=opensearch'>
						<Text style={Feeds.text}>{contact.name}</Text>
					</Link>
					<Link url={'mail://' + contact.email}>
						<Text style={Feeds.text}>{contact.email}</Text>
					</Link>
					<Link url={'tel://' + contact.phone}>
						<Text style={Feeds.text}>{contact.phone}</Text>
					</Link>
				</Animated.View>
			</TouchableOpacity>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: state.feedItem.fetching
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		followRequest: (type, id, followStates) => dispatch(FeedItemActions.followRequest(type, id, followStates))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedItemEventRenderer)
