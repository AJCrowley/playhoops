// @flow

import React, { Component } from 'react'
import {
	Text,
	View
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { NavigationTabs } from '../styles'

export default class NavigationTab extends Component {
	render() {
		return (
			<View style={[NavigationTabs.container]}>
				<Icon name={this.props.iconName} style={[NavigationTabs.icon, this.props.selected ? NavigationTabs.selected : null]} />
				<Text style={[NavigationTabs.text, this.props.selected ? NavigationTabs.selected : null]}>{this.props.title}</Text>
			</View>
		)
	}
}
