// @flow

import React from 'react'
import {
	View,
	TextInput,
	StyleSheet,
	Animated,
	Easing
} from 'react-native'
import Globals from '../config/GlobalSettings'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Colors } from '../styles'

class textInputValidate extends React.Component {
	validators:Object = {}

	state:Object = {
		text: null,
		errors: [],
		errorAnim: null,
		isValid: true
	}
	
	constructor(props:Object) {
		// pass props to owner
		super(props)
		// get validators as array
		this.validators = props.validators
		if(this.validators.validators) {
			this.validators.validators = Array.isArray(this.validators.validators) ? this.validators.validators : [this.validators.validators]
		} else {
			this.validators.validators = []
		}
		this.state = {
			text: this.props.value,
			errors: [],
			errorAnim: new Animated.Value(0),
			isValid: true
		}
		
	}

	componentWillReceiveProps(newProps:Object) {
		this.refs['textInput'].editable = newProps.editable
		this.setState({ text: newProps.value })
		//this.forceUpdate()
	}
	
	focus = () => this.refs['textInput'].focus()

	handleChange = (text:string) => {
		// update state
		this.setState({ text })
		// were we passed an onchangetext function?
		if(typeof this.props.onChangeText === 'function') {
			// call it
			this.props.onChangeText(text)
		}
	}
	
	handleFocus = () => {
		// reset valid to true when focused
		this.setState({ isValid: true })
		// were we passed an onfocus function?
		if(typeof this.props.onFocus === 'function') {
			// call it
			this.props.onFocus()
		}
	}
	
	handleBlur = () => {
		// call validation
		this.validate()
		// were we passed an onblur function?
		if(typeof this.props.onBlur === 'function') {
			// call it, along with the validation result
			this.props.onBlur(this.state.isValid)
		}
	}
	
	validate = () => {
		// default valid to true
		let valid = true
		// default errors to empty array
		const errorMessages = []
		// do we have both a value (or is field required) and some validators?
		if((this.state.text !== '' && this.validators.length) || this.validators.required) {
			// loop over all validators
			for(let validator of this.validators.validators) {
				// is it invalid?
				if(!new RegExp(validator.regex).test(this.state.text)) {
					// set valid to false, we won't need to set this back to true as it represents all tests
					valid = false
					// push current error to stack
					errorMessages.push(validator.error + '\n')
				}
			}
		}
		// update valid and errors states
		this.setState({
			isValid: valid,
			errors: errorMessages
		})
		// return result for anyone interested
		return valid
	}
	
	invalidate = (message:string) => {
		const errorMessages = [message]
		this.setState({
			isValid: false,
			errors: errorMessages
		})
	}
	
	render() {
		const { textInputStyle, errorTextStyle, errorInputStyle, icon, iconStyle, style } = this.props
		const { text, isValid, errorAnim, errors } = this.state
		const styles = [textInputStyle]

		let errorText = null
		if(!isValid) {
			const animHeight = errorAnim.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 18]
			})
			errorText = (
				<Animated.Text style={[defaultStyles.errors, errorTextStyle, {height: animHeight, opacity: errorAnim }]}>
					{errors}
				</Animated.Text>
			)
			Animated.timing(
				errorAnim,
				{
					toValue: 1,
					duration: Globals.animation.baseDuration,
					easing: Easing.inout
				}
			).start()
			styles.push(errorInputStyle)
		}
		const renderIcon = icon ? <Icon name={icon} style={[defaultStyles.icon, iconStyle]} /> : null
		return (
			<View style={style}>
				{ renderIcon }
				<TextInput
					{...this.props}
					ref="textInput"
					style={styles}
					value={text}
					onChangeText={this.handleChange}
					onFocus={this.handleFocus}
					onBlur={this.handleBlur} />
				{errorText}
			</View>
		)
	}
}

const defaultStyles = StyleSheet.create({
	errors: {
		color: 'red'
	},
	icon: {
		color: '#ccc',
		flex: 1,
		fontSize: 20,
		padding: 8,
		position: 'absolute',
		right: 0,
		textAlign: 'center',
		top: 2,
		width: 40
	}
})

export default textInputValidate
