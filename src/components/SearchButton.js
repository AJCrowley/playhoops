// @flow

import React, { Component } from 'react'
import {
	TextInput,
	View
} from 'react-native'
import Icon  from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import { NavBarStyle } from '../styles'

//Note about onPress(): This will navigate directly to search, but the back button will be to return to
//the homeTab, not wherever the user pressed from. This is an issue with react-native-router-flux and tabs.
//Tabs each have a brand new navigation state by default.
const SearchButton = (props:Object) => (
	<View style={[NavBarStyle.buttonWrapper, NavBarStyle.right]}>
		<Icon
			name="ios-search"
			onPress={() => {Actions.homeTab(); Actions.search()}}
			style={NavBarStyle.button}
		/>
	</View>
)

export default SearchButton
