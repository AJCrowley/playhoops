// @flow

import React, { Component } from 'react'

export default class Button extends Component {
	state:Object = {
		pressStatus: false
	}
	
	constructor(props:Object) {
		super(props)
		this.state = {
			pressStatus: false
		}
	}

	setStatus = (status:boolean) => {
		this.setState({
			pressStatus: status
		})
	}
}
