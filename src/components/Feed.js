// @flow

import React, { Component } from 'react'
import {
	ListView,
	RefreshControl,
	StyleSheet,
	Text,
	View
} from 'react-native'
import FeedItem from './FeedItem'
import ListViewSectionHeader from './ListViewSectionHeader'
import Globals from '../config/GlobalSettings.js'
import {
	Feeds,
	Metrics
} from '../styles'

export default class Feed extends Component {
	state:Object = {
		dataSource: null
	}

	ds:Object = {}

	constructor(props:Object) {
		super(props)
		// prepare data source for listview
		this.ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2,
			sectionHeaderHasChanged : (s1, s2) => s1 !== s2
		})
		// default datasource to null
		let dataSource = null
		// do we have data?
		if(props.data) {
			// yes, format the data for the ListView
			const { dataBlob, sectionIds, rowIds } = this.formatData(props.data)
			// populate the data source
			dataSource = this.ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds)
		}
		// set state
		this.state = {
			dataSource: dataSource
		}
	}

	formatData = (data:Object) => {
		const dataBlob = {}
		const rowIds = []
    	const sections = []
		// map out sections
		const sectionIds = Object.keys(data).map((section) => section)
		// loop through sections
		sectionIds.forEach((sec) => {
			// map rows
			let sectionRowIds = Object.keys(data[sec]).map((secDetails) => `${sec}:${secDetails}`)
			// build row ids
			rowIds.push(sectionRowIds)
			// loop through sections
			Object.keys(data[sec]).forEach((secDetail) => {
				// create section in blob
				dataBlob[sec] = {}
				// loop through all rows
				sectionRowIds.forEach((sectionRowId) => {
					// populate row in blob
					dataBlob[sec][sectionRowId] = Object.assign({ type: sec }, data[sec][secDetail])
				})
			})
		})
		// return results
		return { dataBlob, sectionIds, rowIds }
	}

	componentWillReceiveProps(newProps:Object) {
		// did we get some results?
		if(newProps.data) {
			// format the data for the ListView
			const { dataBlob, sectionIds, rowIds } = this.formatData(newProps.data)
			// set state
			this.setState({
				dataSource: this.ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds)
			})
		}
	}

	refreshControlRender = () => (
		<RefreshControl
			refreshing={this.props.isRefreshing}
			onRefresh={() => { this.props.onRefresh() }} />
	)

	render() {
		const { dataSource }:Object = this.state
		return dataSource ? (
			<ListView
				dataSource={dataSource}
				refreshControl={this.props.allowRefresh ? this.refreshControlRender() : null}
				renderSectionHeader={this.props.showHeaders ? (section, category) => <ListViewSectionHeader section={section} category={category} /> : null}
				renderSeparator={(sectionId, rowId) => <View key={rowId} style={Feeds.separator} />}
				renderRow={(item) => <FeedItem {...item} />}
				enableEmptySections={false}
				pageSize={Globals.search.resultsPageSize} />
		) : (
			<Text>No results</Text>
		)
	}
}
