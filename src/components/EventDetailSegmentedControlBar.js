import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { SegmentedControls } from 'react-native-radio-buttons'
import EventDetailsActions from '../redux/EventDetailsRedux'
import Globals from '../config/GlobalSettings'
import {
	Colors,
	ComponentStyle,
	Metrics
} from '../styles'

class EventDetailSegmentedControlBar extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentWillUnmount() {
		//When leaving event details we must reset the active tab index to 0 so that returning
		//to event details will properly return to the Schedule tab for a new division/event
		if(this.props.activeTab !== 0){
			this.props.setActiveTab(0)
		}

	}

	render() {
		//Reduce over tabList defined in redux, returning an array of titles only
		//find this in render in the event that dynamic tabList is ever used
		const tabList = this.props.tabList.reduce((pv, cv) => {
			pv.push(cv.title)
			return pv
		}, [])
		const currentTabIndex = this.props.activeTab

		return (
			<View style={ComponentStyle.segmentedView}>
				<SegmentedControls
					{...Globals.segmentedStyleProps}
					options={tabList}
					selectedOption={tabList[currentTabIndex]}
					onSelection={ (selectedTitle, selectedIndex) => this.props.setActiveTab(selectedIndex) }
				/>
			</View>
		)
	}
/*tint: tint,
      selectedTint: backTint,
      backgroundColor: backTint,
      selectedBackgroundColor: tint,
      containerBorderTint: tint,
      separatorTint: tint,
	   flexDirection: config.direction,
      backgroundColor: config.backgroundColor,
      borderColor: config.containerBorderTint,
      borderWidth: config.containerBorderWidth,
      overflow: 'hidden'
	  baseContainerStyle.borderRadius = config.containerBorderRadius;

    const containerStyle = [baseContainerStyle, this.props.containerStyle];*/
}


const mapStateToProps = (state, ownProps) => {
	return {
		activeTab: state.eventDetails.activeTab,
		tabList: state.eventDetails.tabList,
		eventId: state.eventId
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setActiveTab: (newActiveTab) => dispatch(EventDetailsActions.setEventDetailActiveTab(newActiveTab))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetailSegmentedControlBar)
