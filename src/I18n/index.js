// @flow

import I18n from 'react-native-i18n'

// Enable fallbacks if you want `en-US` and `en-GB` to fallback to `en`
I18n.fallbacks = true
// Get missing translations from
I18n.defaultLocale = 'en'
// Manually set locale for testing
// I18n.locale = 'fr'
// Set translation files
I18n.translations = {
	en: require('./en.json'),
	fr: require('./fr.json')
}
