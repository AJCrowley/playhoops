// @flow

import apisauce from 'apisauce'
import Config from 'react-native-config'
import I18n from 'react-native-i18n'

// can test with https://jsonplaceholder.typicode.com/post

const create = (baseURL:string = Config.API_URL) => {
	const console:Object = {}

	// create API instance on baseURL
	const api = apisauce.create({
		baseURL,
		headers: {
			'Cache-Control': 'no-cache',
			'api_key': Config.API_KEY,
			'lang': I18n.currentLocale()
		},
		timeout: 10000
	})
	// set console output in dev mode
	if (__DEV__ && console.tron) {
		api.addMonitor(console.tron.apisauce)
	}
	// login function
	const login = ({ email, password }:Object) => api.post('login', { email, password })

	const resetPassword = ({ email }:Object) => api.post('resetpassword', { email })

	const signup = ({ firstName, lastName, email, password }:Object) => api.post('signup', { firstName, lastName, email, password })

	const search = ({ searchText, searchTypes }:Object) => api.post('search', { searchText, searchTypes })

	const follow = ({ type, id, followStatus }:Object) => api.post('follow', { type, id, followStatus })

	const events = () => api.post('events')

	const teams = () => api.post('teams')

	//Note api v1.1 does not specify the option to pass to embed
	//Final api design decision still to be decided 2.24.17
	const fetchDivisions = ({ eventId }:Object) => api.get(`events/${eventId}`, { embed: 'divisions' })

	const fetchStandings = ({ eventId, divisionId }:Object) => api.get(`standings/${eventId}/${divisionId}`)

	const fetchGames = ({ eventId, divisionId }:Object) => api.get(`games/${eventId}/${divisionId}`)

	const bracket = ({ eventId, divisionId }:Object) => api.get(`bracket/${eventId}/${divisionId}`)

	// return reduced interface
	return {
		login,
		resetPassword,
		signup,
		search,
		follow,
		events,
		teams,
		fetchDivisions,
		fetchStandings,
		fetchGames,
		bracket
	}
}

// default method
export default {
	create
}
