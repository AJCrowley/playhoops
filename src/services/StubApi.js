// @flow

export default {
	login: function * ({ username, password }:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/login.json')
		}
	},
	signup: function * ({ firstName, lastName, email, password }:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/signup_error.json')
		}
	},
	resetPassword: function * ({ email }:Object):Generator<Promise<any>, Object, void> {
		yield sleep(500)
		return {
			ok: true
		}
	},
	search: function * ({ searchText, searchTypes }:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		let dataFile = require('../stub/search.json')
		if(Array.isArray(searchTypes)) {
			if(searchTypes.length === 1) {
				dataFile = searchTypes.includes('events') ? require('../stub/events.json') : require('../stub/teams.json')
			}
		}
		return {
			ok: true,
			data: dataFile
		}
	},
	follow: function * ({ type:string, id:int, followStatus:boolean }:Object):Generator<Promise<any>, Object, void> {
		yield sleep(500)
		return {
			ok: true
		}
	},
	events: function * ():Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/events.json')
		}
	},
	teams: function * ():Generator<Promise<any>, Object, void> {
		yield sleep(500)
		return {
			ok: true,
			data: require('../stub/teams.json')
		}
	},
	fetchDivisions: function * ({eventId}:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/divisions.json')
		}
	},
	fetchStandings: function * ({eventId, divisionId}:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/standings.json')
		}
	},
	fetchGames: function * ({eventId, divisionId}:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/eventGames.json')
		}
	},
	bracket: function * ({eventId, divisionId}:Object):Generator<Promise<any>, Object, void> {
		yield sleep(1000)
		return {
			ok: true,
			data: require('../stub/bracket.json')
		}
	}
}

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))
