export default SETTINGS = {
	useStubData: __DEV__,
	yellowBox: __DEV__,
	reduxLogging: __DEV__,
	includeExamples: __DEV__
}
