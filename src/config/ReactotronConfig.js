// @flow

import Immutable from 'seamless-immutable'
import R from 'ramda'
import Reactotron, {
	asyncStorage,
	networking,
	openInEditor,
	overlay,
	trackGlobalErrors
} from 'reactotron-react-native'
import apisaucePlugin from 'reactotron-apisauce'
import { reactotronRedux } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'

const HIDE_EFFECTS = [
	'REACT_NATIVE_ROUTER_FLUX_FOCUS',
	'REACT_NATIVE_ROUTER_FLUX_REPLACE',
	'REACT_NATIVE_ROUTER_FLUX_REFRESH',
	'REACT_NATIVE_ROUTER_FLUX_PUSH',
	'REACT_NATIVE_ROUTER_FLUX_BACK_ACTION'
]

if(__DEV__) {
	// Clear any old stuff from Reactotron
	Reactotron.configure().connect().clear()
	Reactotron
		.configure({
			name: '🏀 Blue Star Scheduler 🏀'
		})
		.use(trackGlobalErrors({
			// ignore all error frames from react-native
			//veto: (frame) => frame.fileName.indexOf('/node_modules/react-native/') >= 0
		}))
		.use(networking({
			//ignoreContentTypes: /^(image)\/.*$/i
		}))
		.use(asyncStorage({
			//ignore: ['secret']
		}))
		.use(reactotronRedux({
			isActionImportant: action => action.type.endsWith('_ERROR') || action.type.endsWith('_SUCCESS'),
			onRestore: state => Immutable(state),
			except: HIDE_EFFECTS
		}))
		.use(apisaucePlugin())
		.use(sagaPlugin())
		.use(openInEditor())
		.use(overlay())
		.connect()
	// Assign reactotron to console.tron so we don't need to import it on every file
	console.tron = Reactotron
} else {
	// a mock version should you decide to leave console.tron in your codebase
	console.tron = {
		log: () => false,
		warn: () => false,
		error: () => false,
		display: () => false,
		image: () => false
	}
}
