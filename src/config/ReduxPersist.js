import immutablePersistenceTransform from '../services/ImmutablePersistenceTransform'
import { AsyncStorage } from 'react-native'

const REDUX_PERSIST = {
	active: true,
	reducerVersion: '4',
	storeConfig: {
		storage: AsyncStorage,
		// blacklist: ['login'], // reducer keys that you do NOT want stored to persistence here
		whitelist: ['savedLogin'],
		transforms: [immutablePersistenceTransform]
	}
}

export default REDUX_PERSIST
