import {
	Colors,
	ComponentStyle,
	Metrics
} from '../styles'

export default GLOBALS = {
	search: {
		// minimum length of search string before dispatching search action
		minLength: 3,
		resultsPageSize: 15,
		// time to wait (ms) for additional input before dispatching search action
		timeout: 1500
	},
	animation: {
		baseDuration: 400
	},
	segmentedStyleProps: {
		backgroundColor: Colors.themeSlate,
		backTint: Colors.themeSlate,
		containerBorderRadius: Metrics.baseRadius,
		containerBorderWidth: 0,
		optionContainerStyle: ComponentStyle.segmentedContainer,
		optionStyle: ComponentStyle.segmentedOption,
		selectedBackgroundColor: Colors.themeCoal,
		selectedTint: Colors.white,
		separatorTint: Colors.themeSlate,
		tint: Colors.themeGrey
	}
}
