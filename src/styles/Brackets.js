// @flow
import { StyleSheet, Platform } from 'react-native'
import Fonts from './Fonts'
import Colors from './Colors'
import Metrics from './Metrics'

export default StyleSheet.create({
	bracketView: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		width: 800
	},
	column: {
		alignItems: 'center',
		position: 'relative',
		width: Metrics.screenWidth
	},
	controlBar: {
		alignItems: 'center',
		backgroundColor: Colors.themeDarkGlass,
		borderRadius: Metrics.baseRadius * 2,
		flexDirection: 'row',
		height: 40,
		justifyContent: 'space-between',
		marginHorizontal: 100,
	},
	controlBarContainer: {
		bottom: 50,
		justifyContent: 'center',
		width: Metrics.screenWidth
	},
	controlBarIcon: {
		color: Colors.themeGrey,
		fontSize: 24,
		margin: Metrics.basePadding * 2
	},
	gameContainer: {
		borderColor: Colors.themeGrey,
		borderRadius: Metrics.baseRadius * 2,
		borderWidth: 1,
		margin: Metrics.baseMargin,
		padding: Metrics.basePadding,
		width: Metrics.screenWidth - 100
	},
	gameImage: {
		height: 32,
		width: 32
	},
	homeScore: {
		// any additional props for home score
	},
	homeTeam: {
		// any additional props for home team
	},
	horizContainer: {
		height: Metrics.screenHeight - 172,
		width: Metrics.screenWidth
	},
	roundTitle: {
		fontFamily: Fonts.titleFont,
		marginBottom: Metrics.baseMargin * 2,
		textAlign: 'center',
		width: Metrics.screenWidth
	},
	roundTitleView: {
		flex: 1,
		flexDirection: 'row'
	},
	row: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		paddingVertical: Metrics.baseMargin,
		width: Metrics.screenWidth - 100 - Metrics.baseMargin - Metrics.basePadding
	},
	score: {
		fontFamily: Fonts.titleFont,
		position: 'absolute',
		right: 0,
		textAlign: 'center',
		width: 30
	},
	team: {
		fontFamily: Fonts.bodyFont,
		padding: Metrics.basePadding,
		width: Metrics.screenWidth -  100 - 30 - 32 - Metrics.baseMargin - Metrics.basePadding // width - 100 - score width - image width
	},
	text: {
		fontFamily: Fonts.bodyFont
	},
	title: {
		fontFamily: Fonts.titleFont,
		fontSize: 20,
		textAlign: 'center'
	},
	vertContainer: {
		flexGrow: 1,
		overflow: 'visible',
		marginBottom: 52
	},
	visitorScore: {
		// any additional props for visitor score
	},
	visitorTeam: {
		// any additional props for visitor team
	}
})
