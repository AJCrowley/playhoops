// @flow

import { StyleSheet } from 'react-native'
import Colors from './Colors'
import Fonts from './Fonts'
import Metrics from './Metrics'

export default StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.white
	},
	flexCentered: {
		flex: 1,
		alignSelf: 'center'
	},
	form: {
		backgroundColor: Colors.white,
		margin: Metrics.baseMargin,
		borderRadius: Metrics.baseRadius
	},
	inlineRow: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingVertical: Metrics.baseMargin,
		paddingHorizontal: Metrics.baseMargin
	},
	rootView: {
		flex: 1
	},
	row: {
		paddingVertical: Metrics.baseMargin,
		paddingHorizontal: Metrics.baseMargin
	},
	rowLabel: {
		lineHeight: 30,
		color: Colors.charcoal,
		fontFamily: Fonts.bodyFont
	}
})
