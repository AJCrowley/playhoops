// @flow
import { StyleSheet } from 'react-native'
import Colors from './Colors'
import Fonts from './Fonts'
import Metrics from './Metrics'

export default StyleSheet.create({
	container: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		padding: Metrics.baseMargin
	},
	controlContainer: {
		alignItems: 'flex-end',
		flex: 1
	},
	controlIcons: {
		color: Colors.themeSlate,
		fontSize: 32,
		// comment out next line for vertical control layout
		margin: Metrics.baseMargin
	},
	controls: {
		alignItems: 'center',
		flex: 1,
		// comment out next line for vertical control layout
		flexDirection: 'row',
		padding: Metrics.baseMargin
	},
	details: {
		padding: Metrics.baseMargin
	},
	extendedDetails: {
		height: 0,
		marginLeft: Metrics.feedImageWidth + Metrics.baseMargin,
		paddingLeft: Metrics.basePadding,
		paddingRight: Metrics.basePadding
	},
	header: {
		backgroundColor: Colors.themeSlate,
		flex: 0,
		flexDirection: 'row',
		justifyContent: 'center',
		padding: 8
	},
	headerText: {
		color: Colors.white,
		fontFamily: Fonts.titleFont,
		fontSize: 16,
		paddingTop: 2
	},
	image: {
		height: Metrics.feedImageHeight,
		padding: Metrics.baseMargin,
		width: Metrics.feedImageWidth
	},
	separator: {
		backgroundColor: Colors.themeSilver,
		flex: 1,
		height: StyleSheet.hairlineWidth
	},
	text: {
		fontFamily: Fonts.bodyFont
	}
})
