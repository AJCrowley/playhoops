// @flow
const Colors = {
	// standard ios colors
	black: '#000',
	blue: '#007aff',
	darkGrey: '#8e8e93',
	green: '#4cd964',
	medGrey: '#ceced2',
	orange: '#ff9500',
	pink: '#ff2d55',
	purple: '#5856d6',
	red: '#ff3b30',
	silver: '#efeff4',
	teal: '#5ac8fb',
	white: '#fff',
	yellow: '#ffcc00',
	// theme colors
	themeBlood: '#d30212',
	themeBlue: '#227fef',
	themeCoal: '#222222',
	themeGrey: '#aebac1',
	themeRed: '#f02029',
	themeSilver: '#717a80',
	themeSlate: '#353535',
	themeDarkGlass: 'rgba(0, 0, 0, 0.75)',
	// other colors
	bloodOrange: '#fb5f26',
	charcoal: '#595959',
	cloud: 'rgba(200,200,200, 0.35)',
	coal: '#2d2d2d',
	drawer: 'rgba(30, 30, 29, 0.95)',
	ember: 'rgba(164, 0, 48, 0.5)',
	error: '#ff3b30',
	error: 'rgba(200, 0, 0, 0.8)',
	facebook: '#3b5998',
	fire: '#e73536',
	frost: '#D8D8D8',
	panther: '#161616',
	ricePaper: 'rgba(255,255,255, 0.75)',
	snow: 'white',
	steel: '#CCCCCC',
	transparent: 'rgba(0,0,0,0)',
	windowTint: 'rgba(255, 255, 255, 0.3)'
}

export default Colors
