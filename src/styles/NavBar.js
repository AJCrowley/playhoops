// @flow
import { StyleSheet, Platform } from 'react-native'
import Fonts from './Fonts'
import Colors from './Colors'
import Metrics from './Metrics'

export default StyleSheet.create({
	button: {
		backgroundColor: Colors.transparent,
		color: Colors.themeSlate,
		fontSize: 28,
		height: 28,
		textAlign: 'center',
		width: 38,
		...Platform.select({
			android: {
				top: 5
			},
		}),
	},
	buttonWrapper: {
		alignItems: 'center',
		backgroundColor: Colors.transparent,
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		padding: Metrics.basePadding,
		position: 'absolute',
		width: 42,
		...Platform.select({
			ios: {
				height: 44,
				top: 20
			},
			android: {
				height: 49,
				top: 0
			},
		}),
	},
	navbar: {
		backgroundColor: Colors.white,
		borderBottomColor: Colors.cloud,
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: (Platform.OS === 'ios') ? 64 : 54,
		left: 0,
		opacity: 0.9,
		paddingTop: 0,
		right: 0,
		top: 0
	},
	right: {
		right: 0
	},
	searchBar: {
		backgroundColor: Colors.white,
		borderColor: Colors.steel,
		borderRadius: Metrics.baseRadius,
		borderWidth: 1,
		color: Colors.themeSlate,
		fontFamily: Fonts.bodyFont,
		fontSize: 12,
		height: 30,
		padding: Metrics.basePadding,
		width: 180,
		...Platform.select({
			ios: {
				marginTop: 7
			},
			android: {
				marginTop: 10
			},
		})
	},
	searchContainer: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		left: 0,
		position: 'absolute',
		right: 0,
		...Platform.select({
			ios: {
				height: 44,
				top: 20
			},
			android: {
				height: 49,
				top: 0
			},
		}),
	},
	title: {
		alignSelf: 'center',
		backgroundColor: Colors.transparent,
		color: Colors.themeSlate,
		fontFamily: Fonts.titleFont,
		fontSize: 16,
		fontWeight: '600',
		height: 16,
		textAlign: 'center',
		width: 180
	},
	titleWrapper: {
		flex: 1,
		justifyContent: 'center',
		left: 0,
		marginTop: 0,
		right: 0,
		...Platform.select({
			ios: {
				top: 20,
				height: 44
			},
			android: {
				top: 5,
				height: 49
			},
		}),
	}
})
