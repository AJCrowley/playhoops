// @flow
import { StyleSheet } from 'react-native'
import Colors from './Colors'
import Fonts from './Fonts'
import Metrics from './Metrics'

export default StyleSheet.create({
	container: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	icon: {
		color: Colors.themeGrey,
		fontSize: 28
	},
	selected: {
		color: Colors.white
	},
	tab: {
		backgroundColor: Colors.themeSlate
	},
	tabbar: {
		opacity: 0.98
	},
	tabSelected: {
		backgroundColor: Colors.themeCoal
	},
	text: {
		color: Colors.themeGrey,
		fontFamily: Fonts.titleFont,
		fontSize: 16,
		fontWeight: 'bold'
	}
})
