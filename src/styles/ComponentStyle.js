// @flow
import { StyleSheet, Platform } from 'react-native'
import Metrics from './Metrics'
import Colors from './Colors'
import Fonts from './Fonts'

export default StyleSheet.create({
	button: {
		backgroundColor: Colors.themeRed,
		borderRadius: Metrics.baseRadius,
		flex: 1,
		justifyContent: 'center',
		padding: Metrics.baseMargin
	},
	buttonActive: {
		backgroundColor: Colors.white,
		borderColor: Colors.themeRed,
		borderStyle: 'solid',
		borderWidth: 1
	},
	buttonDown: {
		backgroundColor: Colors.themeRed
	},
	buttonDownText: {
		color: Colors.white
	},
	buttonText: {
		color: Colors.white,
		flex:1,
		fontFamily: Fonts.titleFont,
		fontSize: 16,
		lineHeight: (Platform.OS === 'ios') ? 26 : 20,
		textAlign: 'center'
	},
	buttonTextActive: {
		color: Colors.themeRed
	},
	buttonWrapper: {
		flexGrow: 1,
		height: 40
	},
	errorText: {
		color: Colors.themeRed,
		height: 18
	},
	rightFacingChevron: {
		alignSelf: 'center',
		color: Colors.medGrey,
		fontSize: 30,
		paddingHorizontal: Metrics.basePadding
	},
	segmentedContainer: {
		height: 30,
		justifyContent: 'center',
		paddingBottom: 0,
		paddingTop: 0
	},
	segmentedOption: {
		fontFamily: Fonts.titleFont,
		fontSize: 14,
		marginTop: 2
	},
	segmentedView: {
		paddingBottom: Metrics.basePadding,
		paddingLeft: Metrics.basePadding,
		paddingRight: Metrics.basePadding
	},
	textInput: {
		borderColor: Colors.steel,
		borderRadius: Metrics.baseRadius,
		borderWidth: 1,
		color: Colors.themeSlate,
		fontFamily: Fonts.bodyFont,
		height: 40,
		padding: Metrics.baseMargin
	},
	textInputError: {
		borderColor: Colors.themeRed
	},
	textInputIcon: {
		color: Colors.steel
	},
	textInputReadonly: {
		color: Colors.themeSilver
	},
	textInputValidate: {
		padding: 0
	}
})
