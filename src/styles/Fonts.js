// @flow
const Fonts = {
	bodyFont: 'Open Sans',
	titleFont: 'Bebas Neue'
}

export default Fonts
