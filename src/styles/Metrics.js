// @flow

import {
	Dimensions,
	Platform
} from 'react-native'

const { height, width } = Dimensions.get('window')
const baseUnit = 8

const Metrics = {
	baseMargin: baseUnit,
	basePadding: baseUnit,
	baseRadius: 4,
	doubleBaseMargin: 2 * baseUnit,
	feedImageHeight: 50,
	feedImageWidth: 50,
	marginHorizontal: baseUnit,
	marginVertical: baseUnit,
	navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
	screenHeight: width < height ? height : width,
	screenWidth: width < height ? width : height,
	statusBarHeight: (Platform.OS === 'ios') ? 20 : 0
}

export default Metrics
