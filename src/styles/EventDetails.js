// @flow
import { StyleSheet } from 'react-native'
import Colors from './Colors'
import Fonts from './Fonts'
import Metrics from './Metrics'

export default StyleSheet.create({
	altRow: {
		backgroundColor: Colors.cloud,
		borderBottomWidth: 0
	},
	errorText: {
		color: Colors.themeRed
	},
	header: {
		paddingHorizontal: Metrics.basePadding
	},
	largeCol: {
		width: Metrics.screenWidth - 180
	},
	listItemContainer: {
		borderBottomColor: Colors.themeSilver,
		borderBottomWidth: StyleSheet.hairlineWidth,
		flex: 1,
		flexDirection: 'row',
		paddingVertical: Metrics.basePadding
	},
	listText: {
		fontFamily: Fonts.bodyFont,
		paddingHorizontal: Metrics.basePadding
	},
	separator: {
		backgroundColor: Colors.cloud,
		height: 37,
		marginVertical: -Metrics.basePadding,
		position: 'relative',
		width: StyleSheet.hairlineWidth
	},
	separatorHeader: {
		height: 34
	},
	seperatorHidden: {
		backgroundColor: Colors.transparent,
		width: 1
	},
	smallCol: {
		textAlign: 'center',
		width: 60
	},
	tableRow: {
		borderBottomWidth: 0
	},
	text: {
		alignSelf: 'center',
		flex: 1,
		fontFamily: Fonts.bodyFont,
		paddingHorizontal: Metrics.basePadding
	}
})
