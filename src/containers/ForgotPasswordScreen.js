// @flow

import React, { Component } from 'react'
import {
	Alert,
	ScrollView,
	View
} from 'react-native'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import LoginActions from '../redux/LoginRedux'
import Button from '../components/Button'
import ActivityButton from '../components/ActivityButton'
import TextInputValidate from '../components/TextInputValidate'
import {
	Colors,
	ComponentStyle,
	LayoutStyle
} from '../styles'

class ForgotPasswordScreen extends Component {
	validators:Object = {}

	isAttempting:boolean = false

	state:Object = {
		email: null,
		animating: false
	}
	
	constructor(props:Object) {
		// pass props to owner
		super(props)
		// set default state
		this.state = {
			email: props.email,
			animating: false
		}
		this.validators = {
			email: {
				valid: false,
				required: true,
				validators: {
					regex: '^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
					error: I18n.t('forgotPasswordScreen.errEmail')
				}
			}
		}
		// set attempting flag
		this.isAttempting = false
	}

	componentWillReceiveProps(newProps:Object) {
		this.setState({
			animating: newProps.fetching
		})
		if(newProps.reset) {
			Alert.alert(null, I18n.t('forgotPasswordScreen.success'), [
    			{text: 'OK', onPress: () => NavigationActions.pop()}
  			])
		} else {
			Alert.alert(I18n.t('forgotPasswordScreen.error'))
		}
	}

	validateForm = () => {
		// default result to true
		let isValid = true
		// loop over each validator
		for (let key of Object.keys(this.validators)) {
			// call validation of textinput, work result into overall
			isValid = (this.refs[key].validate() && isValid)
		}
		// return result
		return isValid
	}

	handleResetPassword = () => {
		if(this.validateForm()) {
			// get props from state
			const { email } = this.state
			// set attempting flag
			this.isAttempting = true
			this.setState({
				animating: true
			})
			// call API
			this.props.resetPassword(this.state.email)
		}
	}

	render() {
		const { email, animating } = this.state
		const { fetching } = this.props
		const editable = !fetching
		const textInputStyle = editable ? [ComponentStyle.textInput] : [ComponentStyle.textInput, ComponentStyle.textInputReadonly]
		return (
			<ScrollView style={[LayoutStyle.container]} keyboardDismissMode="interactive" keyboardShouldPersistTaps="handled">
				<View style={LayoutStyle.form}>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="email"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={email}
							editable={editable}
							keyboardType="email-address"
							returnKeyType="next"
							autoCapitalize="none"
							autoCorrect={false}
							onChangeText={(text) => this.setState({ email: text })}
							onSubmitEditing={() => this.refs.password.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.email}
							icon="envelope"
							iconStyle={ComponentStyle.textInputIcon}
							placeholder={I18n.t('loginScreen.emailAddress')} />
					</View>
					<View style={[LayoutStyle.row]}>
						<ActivityButton
							animating={animating}
							indicatorStyle={ComponentStyle.buttonActivity}
							text={I18n.t('forgotPasswordScreen.resetPassword')}
							clickHandler={this.handleResetPassword}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
				</View>
			</ScrollView>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: false,
		error: state.login.error,
		reset: state.login.reset
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		resetPassword: (email) => dispatch(LoginActions.loginReset(email))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen)
