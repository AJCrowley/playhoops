import React, { Component } from 'react'
import {
	Text,
	View
} from 'react-native'
import { connect } from 'react-redux'
import {
	ComponentStyle,
	LayoutStyle
} from '../styles'

class GameDetailsScreen extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<View style={[LayoutStyle.container]}>
				<Text>{this.props.gameId}</Text>
			</View>

		)
	}

}

// const mapStateToProps = (state, ownProps) => {
//  return {
//    error: state.divisionList.error,
//    results: state.divisionList.results,
//    fetching: state.divisionList.fetching
//  }
// }

const mapDispatchToProps = (dispatch) => {
	return {
		setActiveTab: (newActiveTab) => dispatch(EventDetailsActions.setEventDetailActiveTab(newActiveTab))
	}
}

export default connect(null, mapDispatchToProps)(GameDetailsScreen)
