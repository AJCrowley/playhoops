// @flow

import React from 'react'
import {
	ListView,
	RecyclerViewBackedScrollView,
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View
} from 'react-native'

const TestContent = (props:Object) => {
	const { message, buttonAction }:Object = props
	return (
		<ScrollView contentContainerStyle={{justifyContent: 'center'}} style={{flex: 1, paddingTop: 70}}>
			<Text>This is some test content: {message}</Text>
			<TouchableOpacity onPress={buttonAction}>
				<View>
					<Text>Button</Text>
				</View>
			</TouchableOpacity>
		</ScrollView>
	)
}

export default TestContent
