// @flow

import React, { Component } from 'react'
import {
	Linking,
	View
} from 'react-native'
import {
	ActionConst,
	Actions as NavigationActions,
	Router,
	Scene,
	Switch
} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import Config from 'react-native-config'
import SplashScreen from 'react-native-smart-splash-screen'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import PushNotification from 'react-native-push-notification'

// Scene Imports
import LoginScreen from './LoginScreen'
import ForgotPasswordScreen from './ForgotPasswordScreen'
import SignupScreen from './SignupScreen'
import SearchScreen from './SearchScreen'
import HomeScreen from './HomeScreen'
import TeamsScreen from './TeamsScreen'
import DivisionListScreen from './DivisionListScreen'
import GameDetailsScreen from './GameDetailsScreen'
import EventScheduleScreen from './EventScheduleScreen'
import EventStandingsScreen from './EventStandingsScreen'
import EventTeamsScreen from './EventTeamsScreen'
import EventBracketScreen from './EventBracketScreen'

// Component Imports
import CustomNavBar from '../components/CustomNavBar'
import SearchBar from '../components/SearchBar'
import NavigationTab from '../components/NavigationTab'

// Style Imports
import {
	NavBarStyle,
	NavigationTabs
} from '../styles'

export default class NavigationRouter extends Component {
	handleDeepLink = (e:Object) => {
		// remove bluestar:// from URL
		const route = e.url.replace(/.*?:\/\//g, "")
		// TODO: do something with route
		// Format of route should be bluestar://tab/scene/param
	}

	componentDidMount() {
		// did we launch with a URL?
		Linking.getInitialURL().then((url) => {
			if(url) {
				// handle the URL
				this.handleDeepLink({url})
			}
		})
		// Listen for any URL instructions while loaded
		Linking.addEventListener('url', this.handleDeepLink)
		// set up push notifications - https://github.com/zo0r/react-native-push-notification
		PushNotification.configure({
			// Called when Token is generated (iOS and Android)
			onRegister: (token) => {
				// make API call to register token with account/device
				console.tron.log('TOKEN:', token)
			},
			// (required) Called when a remote or local notification is opened or received
			onNotification: (notification) => {
				console.log('NOTIFICATION:', notification)
			},
			// Android GCM Sender ID.
			senderID: Config.GCM_SENDER_ID,
			// iOS Permissions
			permissions: {
				alert: true,
				badge: true,
				sound: true
			},
			popInitialNotification: false,
			requestPermissions: false
		})
		// animate away the splash screen
		SplashScreen.close({
			animationType: SplashScreen.animationType.scale,
			duration: 250,
			delay: 0,
		})
	}

	componentWillUnmount() {
		// remove URL listener
		Linking.removeEventListener('url', this.handleDeepLink)
	}

	render() {
		// connect router to store
		const ReduxRouter = connect()(Router)
		// Back button
		const BackButton = () => <View style={NavBarStyle.buttonWrapper}><Icon name="ios-arrow-dropleft" style={NavBarStyle.button} onPress={NavigationActions.pop}/></View>
		// Events search bar
		const eventSearchBar = () => <SearchBar onFocus={() => NavigationActions.search({types: ['events']})} placeholder={I18n.t('search.placeholderEvents')} />
		// Teams search bar
		const teamSearchBar = () => <SearchBar onFocus={() => NavigationActions.search({types: ['teams']})} filter="teams" placeholder={I18n.t('search.placeholderTeams')} />
		// Regular search bar
		const searchBar = () => <SearchBar autoFocus={true} onChangeText={(text) => NavigationActions.refresh({searchText: text})} placeholder={I18n.t('search.placeholderEvents')} />

		return (
			<ReduxRouter
				navBar={CustomNavBar}
				navigationBarStyle={NavBarStyle.navbar}
				titleStyle={NavBarStyle.title}
				titleWrapperStyle={NavBarStyle.titleWrapper}
				backButton={BackButton}>
				<Scene key="root">
					<Scene initial key="accountWrapper">
						<Scene key="login" component={LoginScreen} title={I18n.t('loginScreen.title')} />
						<Scene key="signup" component={SignupScreen} title={I18n.t('signupScreen.title')} />
						<Scene key="forgotPassword" component={ForgotPasswordScreen} title={I18n.t('forgotPasswordScreen.title')} />
					</Scene>
					<Scene key="appScenesWrapper"
						tabs={true}
						type={ActionConst.REPLACE}
						style={NavigationTabs.tabbar}
						iconStyle={NavigationTabs.tab}
						selectedIconStyle={NavigationTabs.tabSelected}
						pressOpacity={0.8}>
						<Scene initial key="homeTab" title={I18n.t('navigationTabs.home')} icon={NavigationTab} iconName="ios-home-outline" type={ActionConst.REFRESH}>
							<Scene initial key="home" renderTitle={eventSearchBar} component={HomeScreen} title="Home" parentTab="homeTab" />
							<Scene key="search" renderTitle={searchBar} component={SearchScreen} parentTab="homeTab" />
							<Scene key="divisionList" getTitle={(state) => {return (state.navBarTitle) ? state.navBarTitle : ''}} component={DivisionListScreen} parentTab="homeTab" />
							<Scene
								key="eventDetails"
								tabs={true}
								parentTab="homeTab"
								component={
									connect((state, props) => ({
										tabIndex:state.eventDetails.activeTab,
										tabList:state.eventDetails.tabList
									}))(Switch)
								}
								selector={(props) => {
									const { tabIndex, tabList } = props
									return tabIndex && tabList ? tabList[tabIndex].tabKey : "eventScheduleWrapper"
								}}>
								<Scene initial type={ActionConst.REFRESH} key="eventScheduleWrapper" parentTab="homeTab">
									<Scene initial key="eventSchedule" getTitle={(state) => (state.navBarTitle) ? state.navBarTitle : ''} component={EventScheduleScreen} parentTab="homeTab" />
									<Scene key="gameDetails" getTitle={(state) => (state.navBarTitle) ? state.navBarTitle : ''} component={GameDetailsScreen} parentTab="homeTab" />
								</Scene>
								<Scene type={ActionConst.REFRESH} key="eventTeams" getTitle={(state) => (state.navBarTitle) ? state.navBarTitle : ''} component={EventTeamsScreen} parentTab="homeTab" />
								<Scene type={ActionConst.REFRESH} key="eventStandings" getTitle={(state) => (state.navBarTitle) ? state.navBarTitle : ''} component={EventStandingsScreen} parentTab="homeTab" />
								<Scene type={ActionConst.REFRESH} key="eventBracket" getTitle={(state) => (state.navBarTitle) ? state.navBarTitle : ''} component={EventBracketScreen} parentTab="homeTab" />
							</Scene>
						</Scene>
						<Scene key="teamsTab" title={I18n.t('navigationTabs.myTeams')} icon={NavigationTab} iconName="ios-shirt-outline" type={ActionConst.REFRESH}>
							<Scene key="myTeams" renderTitle={teamSearchBar} component={TeamsScreen} title="My Teams" parentTab="teamsTab" />
						</Scene>
					</Scene>
				</Scene>
			</ReduxRouter>
		)
	}
}
