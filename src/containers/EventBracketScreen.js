// @flow

import React, { Component } from 'react'
import {
	Text,
	View
} from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import BracketActions from '../redux/BracketRedux'
import BracketsDisplay from '../components/BracketsDisplay'
import LoadContainer from '../components/LoadContainer'
import EventDetailSegmentedControlBar from '../components/EventDetailSegmentedControlBar'
import {
	Brackets,
	EventDetails,
	LayoutStyle
} from '../styles'

class BracketScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		this.props.loadBracket(this.props.eventId, this.props.divisionId)
	}

	render() {
		const { bracket, fetching } = this.props
		return (
			<View style={[LayoutStyle.container]}>
				<EventDetailSegmentedControlBar />
				<LoadContainer fetching={fetching} style={LayoutStyle.flexCentered}>
				{
					(bracket && bracket.PlayoffBracket.tree_info)
					? <BracketsDisplay tree={bracket.PlayoffBracket.tree_info[0]} />
					: <Text style={[Brackets.text, EventDetails.errorText]}>{I18n.t('eventBracketScreen.noResults')}</Text>
				}
				</LoadContainer>
			</View>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: state.bracket.fetching,
		bracket: state.bracket.results
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		loadBracket: (eventId, divisionId) => dispatch(BracketActions.bracketRequest(eventId, divisionId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(BracketScreen)
