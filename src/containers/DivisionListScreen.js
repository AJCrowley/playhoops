// @flow

import React, { Component } from 'react'
import {
	ScrollView,
	Text,
	TouchableOpacity,
	View
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import Icon from 'react-native-vector-icons/Ionicons'
import DivisionListActions from '../redux/DivisionListRedux'
import NavigatorActions from '../redux/NavigatorRedux'
import {
	ComponentStyle,
	EventDetails,
	Feeds,
	LayoutStyle
} from '../styles'

//Components
import DivisionItem from '../components/DivisionItem'
import LoadContainer from '../components/LoadContainer'

class DivisionListScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		const { id, bannerImage, setHeaderImage, fetchDivisionList, dispatchDivisionError } = this.props
		// If event id is passed then attempt division list search
		if(id){
			if(bannerImage) {
				setHeaderImage(bannerImage)
			}
			fetchDivisionList(id)
		} else {
			dispatchDivisionError({
				error: {
					message: I18n.t('divisionListScreen.noEventId')
				}
			})
		}
	}

	componentWillUnmount() {
		this.props.clearHeaderImage()
	}

	/*
		Render logic:
		-> check fetching
		-> check error
		-> check results exist
	*/
	render() {
		const { fetching, error } = this.props
		return (
			<View style={[LayoutStyle.container]}>
				<View style={Feeds.header}>
					<Text style={Feeds.headerText}>{I18n.t('divisionListScreen.divisionTitle')}</Text>
				</View>
				<LoadContainer fetching={fetching} style={LayoutStyle.flexCentered}>
					{
						(!error)
							? <DivisionFeed {...this.props} />
							: <Text style={[EventDetails.text]}>{error.message}</Text>
					}
				</LoadContainer>
			</View>
		)
	}
}

/* Division Feed */
const DivisionFeed = ({ results, navBarTitle }) => {
	return (
		(results && results.divisions.length > 0)
		? <ScrollView
				keyboardDismissMode="interactive"
				keyboardShouldPersistTaps="handled">
				{//map through list of divisions, Navigate to eventDetails route specifying division and new title
					results.divisions.map((cv, index) =>
					<DivisionItem
						key={index}
						name={cv.name}
						onDivisionPress={() => {
							Actions.eventDetails({divisionId:cv.division_id, eventId: results.event_id, navBarTitle:`${navBarTitle} / ${cv.name}`})}
						}
					/>
				)}
			</ScrollView>
		: <Text style={[EventDetails.text]}>{I18n.t('divisionListScreen.noResults')}</Text>
	)
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	return {
		error: state.divisionList.error,
		results: state.divisionList.results,
		fetching: state.divisionList.fetching
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	return {
		setHeaderImage: (headerImage) => dispatch(NavigatorActions.setHeaderImage(headerImage)),
		clearHeaderImage: () => dispatch(NavigatorActions.clearHeaderImage()),
		fetchDivisionList: (eventId) => dispatch(DivisionListActions.divisionListRequest(eventId)),
		dispatchDivisionError: (error) => dispatch(DivisionListActions.divisionListError(error))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(DivisionListScreen)
