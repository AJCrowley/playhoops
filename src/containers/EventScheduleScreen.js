import React, { Component } from 'react'
import {
	ListView,
	Text,
	View
} from 'react-native'
import { Actions as NavigationActions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { SegmentedControls } from 'react-native-radio-buttons'
import EventDetailSegmentedControlBar from '../components/EventDetailSegmentedControlBar'
import LoadContainer from '../components/LoadContainer'
import Feed from '../components/Feed'
import EventDetailsActions from '../redux/EventDetailsRedux'
import Globals from '../config/GlobalSettings'
import {
	ComponentStyle,
	LayoutStyle
} from '../styles'

class EventScheduleScreen extends Component {
	constructor(props:Object) {
		super(props)
		this.state = {
			selectedTab: 0,
			isRefreshing: false,
			resultsTabTitles: [I18n.t('eventScheduleScreen.resultTypeUpcoming'), I18n.t('eventScheduleScreen.resultTypeResults')],
		}
	}

	componentDidMount() {
		const { eventId, divisionId }:Object = this.props
		if(eventId && divisionId) {
			this.props.fetchGames(eventId, divisionId)
		} else {
			Alert.alert(I18n.t('general.error'), I18n.t('eventStandingsScreen.errNoId'))
		}
	}

	componentWillReceiveProps(nextProps, nextState) {
		/**
		 * If we are currently refreshing and nextProps indicates that our fetch to the server has ended we can now
		 * determine that we are done refreshing. NOTE: we must use refresh as well as fetching in order to determine
		 * the difference between the initial load (which will use container spinner) and subsequent refreshes
		 * that will use the <ListView> refresh indicator.
		 */
		if(this.state.isRefreshing && !nextProps.fetching) {
			this.setState({ isRefreshing: false })
		}
	}
	/**
	 * FIXME: status from api yaml has yet to be explained, may need to check date instead
	 * ASSUME: Status 1 means in progress
	 * If status 0 only means not in progress then this will require checking of dates to determine if the game
	 * is finished or yet to be played
	 */
	splitResultsFromUpcomingGames = (gameList:Array, tabIndex:Number) => {
		// tab index 1 is Results tab
		if(tabIndex === 1) {
			// Filter naively on status
			const filtered = gameList.filter((cv) => cv.status !== 0)
			// Results order needs to be flipped to desc based on date and assumption that api returns asc date order
			const ordered = [...filtered].reverse()
			return ordered
		} else {
			return gameList.filter((cv) => cv.status === 0)
		}
	}

	/**
	 * Some additional information may need to be injected into an item in order for the <ListView>
	 * render item to contain everything required for its render
	 */
	injectAdditionalProps = (filteredList) => {
		const injectedList = filteredList.map((cv) => (
			{ ...cv,
				injectedProps: { navBarTitle: this.props.navBarTitle }
			}
		))
		return injectedList
	}

	// format Data for Feed.js
	formatData = (gameList:Array, tabIndex:Number) => {
		// make sure actual results exist
		if((!gameList || gameList.length < 1)) {
			return {}
		}
		// Split results based on tabIndex of result
		const filteredList = this.splitResultsFromUpcomingGames(gameList, tabIndex)
		// Inject props needed for item renderer (navBarTitle)
		const injectedList = this.injectAdditionalProps(filteredList)
		/**
		 * Reduce through actual items, sorting items into an object with unique date keys from the uniqueDates array
		 * ex. --  {"uniqueDate1": [{},{},{}], "uniqueDate2": [{},{}]}
		 */
		const gameListObject = injectedList.reduce((pv, cv, index) => {
			// check if this key exists in object and assign a new array if not
			if(!pv[cv.day]){
				pv[cv.day] = []
			}
			// push the current item to the array residing at key cv.day in the final object
			pv[cv.day].push(cv)
			return pv
		}, {})
		return gameListObject
	}

	onSelectedTab = (selectedTitle, selectedIndex) => {
		//Avoid setting state if same tab is pressed again
		if(selectedIndex !== this.state.selectedTab) {
			this.setState({ selectedTab: selectedIndex })
		}
	}

	onRefresh = () => {
		const { eventId, divisionId } = this.props
		this.setState({ isRefreshing: true })
		this.props.fetchGames(eventId, divisionId)
	}

	render() {
		const formattedDataBlob = this.formatData(this.props.gameList, this.state.selectedTab)
		const tabTitles = this.state.resultsTabTitles
		// If this is initial fetch then render LoadContainer spinner, else if this is pull to refresh then use <RefreshControl>
		const initialFetch = this.props.fetching && !this.state.isRefreshing
		return (
			<View style={[LayoutStyle.container]}>
				<EventDetailSegmentedControlBar />
				<LoadContainer fetching={initialFetch} style={LayoutStyle.flexCentered}>
					<View>
						<View style={ComponentStyle.segmentedView}>
							<SegmentedControls
								{...Globals.segmentedStyleProps}
								options={tabTitles}
								selectedOption={tabTitles[this.state.selectedTab]}
								onSelection={this.onSelectedTab} />
						</View>
					{
						(!this.props.gameListError)
							? <GameFeed
									data={formattedDataBlob}
									tabTitles={tabTitles}
									currentTab={this.state.selectedTab}
									onRefresh={this.onRefresh}
									isRefreshing={this.state.isRefreshing} />
							: <Text style={[EventDetails.text, EventDetails.errorText]}>{I18n.t('eventScheduleScreen.gameListError')}</Text>
					}
					</View>
				</LoadContainer>
			</View>
		)
	}
}

const GameFeed = ({ data, currentTab, tabTitles, onRefresh, isRefreshing }:Object) => {
	if(data && Object.keys(data).length > 0) {
		return (
			<Feed
				data={data}
				showHeaders={true}
				allowRefresh={true}
				onRefresh={onRefresh}
				isRefreshing={isRefreshing} />
		)
	} else {
		return (
			<Text>{tabTitles[currentTab]}</Text>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	return {
		gameList: state.eventDetails.gameList,
		fetching: state.eventDetails.fetchingGames,
		gameListError: state.eventDetails.gameListError
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	return {
		fetchGames: (eventId, divisionId) => dispatch(EventDetailsActions.fetchGamesRequest(eventId, divisionId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(EventScheduleScreen)
