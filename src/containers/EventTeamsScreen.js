// @flow

import React, { Component } from 'react'
import {
	Text,
	View
} from 'react-native'
import { LayoutStyle } from '../styles'
import EventDetailSegmentedControlBar from '../components/EventDetailSegmentedControlBar'

class EventTeamsScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	render() {
		return (
			<View style={[LayoutStyle.container]}>
				<EventDetailSegmentedControlBar />
				<Text >
					{'TODO Event Teams'}
				</Text>
			</View>
		)
	}
}

export default EventTeamsScreen
