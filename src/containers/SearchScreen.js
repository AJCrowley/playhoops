// @flow

import React, { Component } from 'react'
import { KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import SearchActions from '../redux/SearchRedux'
import Globals from '../config/GlobalSettings.js'
import Feed from '../components/Feed'
import { LayoutStyle } from '../styles'

class SearchScreen extends Component {
	state:Object = {
        timer: null,
		searchText: null
    }

	constructor(props:Object) {
		super(props)
		this.state = {
			timer: null,
			searchTypes: props.types,
			searchText: props.searchText
		}
	}

	componentWillReceiveProps(newProps:Object) {
		const { searchText } = newProps
		const { searchTypes } = this.state
		// check if search text has changed
		if(this.state.searchText !== newProps.searchText) {
			this.setState({
				searchText: newProps.searchText,
			})
			// do we have an updated search?
			if(searchText) {
				// is a timer already set?
				if(this.state.timer) {
					// clear old timeout
					this.setState({
						timer: clearTimeout(this.state.timer)
					})
				}
				// is our search at least X chars long as defined by Globals?
				if(searchText.length >= Globals.search.minLength) {
					// set a timer to wait for further input, wait based on timeout value in Globals
					this.setState({
						timer: setTimeout(() => {
							// dispatch search action
							this.props.attemptSearch(searchText, searchTypes)
							// clear old timeout data
							this.setState({
								timer: clearTimeout(this.state.timer)
							})
						}, Globals.search.timeout)
					})
				} else {
					// we don't have min search length, reset the results if necessary
					if(this.props.results) {
						this.props.searchClear()
					}
				}
			}
		}
	}

	render() {
		const { searchText, results } = this.props
		return (
			<KeyboardAvoidingView
				style={[LayoutStyle.container]}
				behavior="padding">
				<Feed data={results} showHeaders={true} />
			</KeyboardAvoidingView>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: state.search.fetching,
		results: state.search.results
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		attemptSearch: (searchText, searchTypes) => dispatch(SearchActions.searchRequest(searchText, searchTypes)),
		searchClear: () => dispatch(SearchActions.searchClear())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)
