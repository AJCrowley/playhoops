// @flow

import React, { Component } from 'react'
import {
	StatusBar,
	View
} from 'react-native'
import { LayoutStyle } from '../styles'
import NavigationRouter from './NavigationRouter'
import { connect } from 'react-redux'
import StartupActions from '../redux/StartupRedux'
import ReduxPersist from '../config/ReduxPersist'

class RootContainer extends Component {
	componentDidMount () {
		// if redux persist is not active fire startup action
		if (!ReduxPersist.active) {
			this.props.startup()
		}
	}

	render () {
		return (
			<View style={LayoutStyle.rootView}>
				<StatusBar barStyle="dark-content" />
				<NavigationRouter />
			</View>
		)
	}
}

const mapDispatchToProps = (dispatch) => ({
	startup: () => dispatch(StartupActions.startup())
})

//Note null mapStateToProps, perhaps startup saved login credentials could be mapped here and passed to nav router?
export default connect(null, mapDispatchToProps)(RootContainer)
