// @flow

import React, { Component } from 'react'
import {
	Text,
	View,
	Alert
} from 'react-native'
import { connect } from 'react-redux'
import EventsActions from '../redux/EventsRedux'
import LoadContainer from '../components/LoadContainer'
import Feed from '../components/Feed'
import {
	LayoutStyle,
	Metrics
} from '../styles'

class HomeScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		this.props.loadEvents()
	}

	render() {
		const { events, fetching } = this.props
		return (
			<View
				style={LayoutStyle.container}
				keyboardDismissMode="interactive"
				keyboardShouldPersistTaps="always">
				<LoadContainer fetching={fetching} style={LayoutStyle.flexCentered}>
					<Feed data={events} />
				</LoadContainer>
			</View>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: state.events.fetching,
		events: state.events.results
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		loadEvents: () => dispatch(EventsActions.eventsRequest())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
