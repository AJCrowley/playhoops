// @flow

import React, { Component } from 'react'
import {
	Text,
	View
} from 'react-native'
import { connect } from 'react-redux'
import TeamsActions from '../redux/TeamsRedux'
import LoadContainer from '../components/LoadContainer'
import Feed from '../components/Feed'
import { LayoutStyle } from '../styles'

class TeamsScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		this.props.loadTeams()
	}

	render() {
		const { teams, fetching } = this.props
		return (
			<View
				style={[LayoutStyle.container]}
				keyboardDismissMode="interactive"
				keyboardShouldPersistTaps="always">
				<LoadContainer fetching={fetching} style={LayoutStyle.flexCentered}>
					<Feed data={teams} showHeaders={false} />
				</LoadContainer>
			</View>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: state.teams.fetching,
		teams: state.teams.results
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		loadTeams: () => dispatch(TeamsActions.teamsRequest())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamsScreen)
