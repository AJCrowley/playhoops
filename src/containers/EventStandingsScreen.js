// @flow

import React, { Component } from 'react'
import {
	Alert,
	ScrollView,
	Text,
	View
} from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import EventDetailsActions from '../redux/EventDetailsRedux'
import EventDetailSegmentedControlBar from '../components/EventDetailSegmentedControlBar'
import LoadContainer from '../components/LoadContainer'
import {
	EventDetails,
	Feeds,
	LayoutStyle
} from '../styles'

class EventStandingsScreen extends Component {
	constructor(props:Object) {
		super(props)
	}

	componentDidMount() {
		const { eventId, divisionId }:Object = this.props
		//If event id is passed then fetch standings
		if(eventId && divisionId) {
			this.props.fetchStandings(eventId, divisionId)
		} else {
			Alert.alert(I18n.t('general.error'), I18n.t('eventStandingsScreen.errNoId'))
		}
	}

	render() {
		const { fetching, error, standings } = this.props
		return (
			<View style={[LayoutStyle.container]}>
				<EventDetailSegmentedControlBar />
				<View style={Feeds.header}>
					<View style={[EventDetails.separator, EventDetails.separatorHeader, EventDetails.seperatorHidden]} />
					<Text style={[Feeds.headerText, EventDetails.largeCol, EventDetails.header]}>{I18n.t('eventStandingsScreen.team')}</Text>
					<View style={[EventDetails.separator, EventDetails.separatorHeader]} />
					<Text style={[Feeds.headerText, EventDetails.smallCol, EventDetails.header]}>{I18n.t('eventStandingsScreen.record')}</Text>
					<View style={[EventDetails.separator, EventDetails.separatorHeader]} />
					<Text style={[Feeds.headerText, EventDetails.smallCol, EventDetails.header]}>{I18n.t('eventStandingsScreen.ptsf')}</Text>
					<View style={[EventDetails.separator, EventDetails.separatorHeader]} />
					<Text style={[Feeds.headerText, EventDetails.smallCol, EventDetails.header]}>{I18n.t('eventStandingsScreen.ptsa')}</Text>
				</View>
				<LoadContainer fetching={fetching} style={LayoutStyle.flexCentered}>
					{
						(!this.props.error)
							? <StandingsList {...standings} />
							: <Text style={[EventDetails.text, EventDetails.errorText]}>{error.message}</Text>
					}
				</LoadContainer>
			</View>
		)
	}
}

/* Standings */
const StandingsList = (props:Object) => {
	const { standings } = props
	return (
		(standings && standings.length > 0)
		? <ScrollView
			keyboardDismissMode="interactive"
			keyboardShouldPersistTaps="handled">
			{//map through list of standings
				standings.map((cv, index) => <StandingsItem key={index} index={index} {...cv} />)
			}
		</ScrollView>
		: <Text style={EventDetails.text}>{I18n.t('eventStandingsScreen.noResults')}</Text>
	)
}

const StandingsItem = ({ team, record, ptsf, ptsa, index }:Object) => {
	const truncNameLength = 25
	return (
		<View style={[EventDetails.listItemContainer, index % 2 ? EventDetails.altRow : EventDetails.tableRow]}>
			<Text style={[EventDetails.listText, EventDetails.largeCol]}>
				{(team.name.length < truncNameLength) ? team.name : team.name.slice(0, truncNameLength) + '...' }
			</Text>
			<View style={EventDetails.separator} />
			<Text style={[EventDetails.listText, EventDetails.smallCol]}>{record}</Text>
			<View style={EventDetails.separator} />
			<Text style={[EventDetails.listText, EventDetails.smallCol]}>{ptsf}</Text>
			<View style={EventDetails.separator} />
			<Text style={[EventDetails.listText, EventDetails.smallCol]}>{ptsa}</Text>
		</View>
	)
}

/*<Icon style={ComponentStyle.rightFacingChevron} name="ios-arrow-forward" />*/
		
const mapStateToProps = (state:Object, ownProps:Object) => {
	return {
		standings: state.eventDetails.standings,
		fetching: state.eventDetails.fetchingStandings
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	return {
		fetchStandings: (eventId, divisionId) => dispatch(EventDetailsActions.fetchStandingsRequest(eventId, divisionId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(EventStandingsScreen)
