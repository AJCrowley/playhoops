// @flow

import React, { Component } from 'react'
import {
	Alert,
	KeyboardAvoidingView,
	ScrollView,
	Text,
	TextInput,
	View
} from 'react-native'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import SignupActions from '../redux/SignupRedux'
import ActivityButton from '../components/ActivityButton'
import Button from '../components/Button'
import TextInputValidate from '../components/TextInputValidate'
import {
	ComponentStyle,
	LayoutStyle
} from '../styles'

class SignupScreen extends Component {
	validators:Object = {}

	isAttempting:boolean = false

	state:Object = {
        firstName: '',
		lastName: '',
		email: '',
		password: '',
		confirmPassword: '',
		passwordsMatch: false,
		animating: false
    }

	constructor(props:Object) {
		// pass props to owner
		super(props)
		// set default state
		/*this.state = {
			firstName: '',
			lastName: '',
			email: '',
			password: '',
			confirmPassword: '',
			passwordsMatch: false,
			animating: false
		}*/
		this.validators = {
			firstName: {
				valid: false,
				required: true,
				validators: {
					regex: '^.{2,25}$',
					error: I18n.t('signupScreen.errFirstName')
				}
			},
			lastName: {
				valid: false,
				required: true,
				validators: {
					regex: '^.{2,25}$',
					error: I18n.t('signupScreen.errLastName')
				}
			},
			email: {
				valid: false,
				required: true,
				validators: {
					regex: '^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
					error: I18n.t('signupScreen.errEmail')
				}
			},
			password: {
				valid: false,
				required: true,
				validators: {
					regex: '^.{2,25}$',
					error: I18n.t('signupScreen.errPasswordLength')
				}
			},
			confirmPassword: {
				valid: false,
				required: true
			}
		}
		// set attempting flag
		this.isAttempting = false
	}

	componentWillReceiveProps(newProps:Object) {
		this.setState({
			animating: newProps.fetching
		})
		// did we get an error?
		if(newProps.error) {
			// does the error have a message?
			if(newProps.error.message) {
				// show alert to user with error message
				Alert.alert(I18n.t('general.error'), newProps.error.message)
				// were any state fields passed to clear?
				if(newProps.error.fields) {
					// create empty object for our updated state
					const newState = {}
					// loop through any passed fields
					for(let field of newProps.error.fields) {
						// build an object to pass to state
						newState[field] = ''
					}
					// set state
					this.setState(newState)
				}
			} else {
				Alert.alert(I18n.t('general.error'), I18n.t('general.serverError'))
			}
		}
		if(newProps.signedUp) {
			Alert.alert(I18n.t('signupScreen.signedUpTitle'), I18n.t('signupScreen.signedUp'), [
    			{text: 'OK', onPress: () => NavigationActions.pop()}
  			])
		}
	}

	checkPasswords = () => {
		const passwordsMatch = this.state.confirmPassword === this.state.password
		// if not show error
		if(!passwordsMatch) {
			// clear confirm password entry from state
			this.setState({
				confirmPassword: '' ,
				passwordsMatch: passwordsMatch
			})
			// show error
			this.refs['password'].invalidate(I18n.t('signupScreen.errPasswordMismatch'))
		} else {
			this.setState({
				passwordsMatch: passwordsMatch
			})
		}
		//return value to calling function in the event calling function requires passwordsMatch immediately
		return passwordsMatch
	}

	validateForm = () => {
		// default result to true
		let isValid = true
		// loop over each validator
		for (let key of Object.keys(this.validators)) {
			// call validation of textinput, work result into overall
			isValid = (this.refs[key].validate() && isValid)
		}
		// check passwords match
		const passwordsMatch = this.checkPasswords()
		// return result
		return isValid && passwordsMatch
	}

	handlePressCancel = () => {
		// pop view back to login
		NavigationActions.pop()
	}

	handlePressSignup = () => {
		// are all fields valid?
		if(this.validateForm()) {
			// get props from state
			const { firstName, lastName, email, password } = this.state
			this.setState({
				animating: true
			})
			// dispatch action
			this.props.attemptSignup(firstName, lastName, email, password)
		}
	}

	render() {
		const { firstName, lastName, email, password, confirmPassword, passwordsMatch, animating } = this.state
		const { fetching } = this.props
		const editable = !fetching
		const textInputStyle = editable ? [ComponentStyle.textInput] : [ComponentStyle.textInput, ComponentStyle.textInputReadonly]
		//const textInputNoPadStyle = editable ? [ComponentStyle.textInputNoPad] : [ComponentStyle.textInputNoPad, ComponentStyle.textInputReadonly]
		return (
			<KeyboardAvoidingView behavior="padding" style={[LayoutStyle.container]}>
				<ScrollView style={LayoutStyle.form}>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="firstName"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={firstName}
							editable={editable}
							keyboardType="default"
							returnKeyType="next"
							autoCapitalize="words"
							autoCorrect={false}
							//onChangeText={this.handleChangeFirstName}
							onChangeText={(text) => this.setState({ firstName: text })}
							onSubmitEditing={() => this.refs.lastName.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.firstName}
							placeholder={I18n.t('signupScreen.firstName')} />
					</View>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="lastName"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={lastName}
							editable={editable}
							keyboardType="default"
							returnKeyType="next"
							autoCapitalize="words"
							autoCorrect={false}
							onChangeText={(text) => this.setState({ lastName: text })}
							onSubmitEditing={() => this.refs.email.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.lastName}
							placeholder={I18n.t('signupScreen.lastName')} />
					</View>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="email"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={email}
							editable={editable}
							keyboardType="email-address"
							returnKeyType="next"
							autoCapitalize="none"
							autoCorrect={false}
							onChangeText={(text) => this.setState({ email: text })}
							onSubmitEditing={() => this.refs.password.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.email}
							icon="envelope"
							iconStyle={ComponentStyle.textInputIcon}
							placeholder={I18n.t('signupScreen.email')} />
					</View>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="password"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={password}
							editable={editable}
							keyboardType="default"
							returnKeyType="next"
							autoCapitalize="none"
							autoCorrect={false}
							secureTextEntry
							onChangeText={(text) => this.setState({ password: text })}
							onSubmitEditing={() => this.refs.confirmPassword.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.password}
							icon="lock"
							iconStyle={ComponentStyle.textInputIcon}
							placeholder={I18n.t('signupScreen.password')} />
					</View>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="confirmPassword"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={confirmPassword}
							editable={editable}
							keyboardType="default"
							returnKeyType="go"
							autoCapitalize="none"
							autoCorrect={false}
							secureTextEntry
							onChangeText={(text) => this.setState({ confirmPassword: text })}
							onSubmitEditing={this.handlePressSignup}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.confirmPassword}
							icon="lock"
							placeholder={I18n.t('signupScreen.confirmPassword')} />
					</View>
					<View style={[LayoutStyle.row]}>
						<ActivityButton
							text={I18n.t('signupScreen.signUp')}
							animating={animating}
							indicatorStyle={ComponentStyle.buttonActivity}
							clickHandler={this.handlePressSignup}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
					<View style={[LayoutStyle.row]}>
						<Button text={I18n.t('signupScreen.cancel')}
							clickHandler={this.handlePressCancel}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
				</ScrollView>
			</KeyboardAvoidingView>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		error: state.signup.error,
		fetching: state.signup.fetching,
		signedUp: state.signup.signedUp
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		attemptSignup: (firstName, lastName, email, password) => dispatch(SignupActions.signupRequest(firstName, lastName, email, password))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen)
