// @flow

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import '../I18n/'
import createStore from '../redux'
import RootContainer from './RootContainer'

const store = createStore()

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<RootContainer />
			</Provider>
		)
	}
}

const BlueStarScheduler = __DEV__ ? console.tron.overlay(App) : App
export default BlueStarScheduler
