// @flow

import React, { Component } from 'react'
import {
	Alert,
	ScrollView,
	Switch,
	Text,
	TextInput,
	View
} from 'react-native'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import LoginActions from '../redux/LoginRedux'
import SavedLoginActions from '../redux/SavedLoginRedux'
import ActivityButton from '../components/ActivityButton'
import Button from '../components/Button'
import TextInputValidate from '../components/TextInputValidate'
import {
	Colors,
	ComponentStyle,
	LayoutStyle
} from '../styles'

class LoginScreen extends Component {
	state:Object = {
		email: '',
		password: '',
		remember: true,
		userData: null,
		savedLogin: null
	}

	validators:Object = {}
	
	isAttempting:boolean = false

	constructor(props:Object) {
		// pass props to owner
		super(props)
		// set default state
		this.state = {
			email: '',
			password: '',
			remember: true,
			userData: null,
			savedLogin: null
		}
		this.validators = {
			email: {
				valid: false,
				required: true,
				validators: {
					regex: '^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
					error: I18n.t('loginScreen.errEmail')
				}
			},
			password: {
				valid: false,
				required: true,
				validators: {
					regex: '^.{2,25}$',
					error: I18n.t('loginScreen.errPasswordLength')
				}
			}
		}
		// set attempting flag
		this.isAttempting = false
	}

	componentWillReceiveProps(newProps:Object) {
		if(newProps.userData) {
			// login success
			NavigationActions.appScenesWrapper()
		}
		this.setState({
			animating: newProps.fetching
		})
		// are we receiving a saved login?
		if(newProps.savedLogin) {
			// get props
			const { email, password, remember } = newProps.savedLogin
			// apply to state
			this.setState({
				email: email,
				password: password,
				remember: remember
			})
		}
		// did we get an error?
		if(newProps.error) {
			// does the error have a message?
			if(newProps.error.message) {
				// show alert to user with error message
				Alert.alert(I18n.t('general.error'), newProps.error.message)
			} else {
				// show generic server error
				Alert.alert(I18n.t('general.error'), I18n.t('general.serverError'))
			}
			// were any state fields passed to clear?
			if(newProps.error.fields) {
				// loop through any passed fields
				for(let field of newProps.error.fields) {
					// set field to empty string
					this.setState({
						[field]: ''
					})
				}
			}
		}
	}

	validateForm = () => {
		// default result to true
		let isValid = true
		// loop over each validator
		for (let key of Object.keys(this.validators)) {
			// call validation of textinput, work result into overall
			isValid = (this.refs[key].validate() && isValid)
		}
		// return result
		return isValid
	}

	handlePressLogin = () => {
		if(this.validateForm()) {
			// get props from state
			const { email, password, remember } = this.state
			// set attempting flag
			this.isAttempting = true
			// do we want to remember login?
			if(remember) {
				// save the login to state
				this.props.saveLogin(email, password, remember)
			} else {
				// save only the remember state, clear other fields
				this.props.saveLogin(null, null, false)
				// make sure saved login state is cleared
				//this.props.clearLogin()
			}
			this.setState({
				animating: true
			})
			// attempt login
			this.props.attemptLogin(email, password, remember)
		}
	}

	render() {
		const { email, password, remember, animating } = this.state
		const { fetching } = this.props
		const editable = !fetching
		const textInputStyle = editable ? [ComponentStyle.textInput] : [ComponentStyle.textInput, ComponentStyle.textInputReadonly]
		return (
			<ScrollView style={[LayoutStyle.container]} keyboardDismissMode="interactive" keyboardShouldPersistTaps="handled">
				<View style={LayoutStyle.form}>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="email"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={email}
							editable={editable}
							keyboardType="email-address"
							returnKeyType="next"
							autoCapitalize="none"
							autoCorrect={false}
							onChangeText={(text) => this.setState({ email: text })}
							onSubmitEditing={() => this.refs.password.focus()}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.email}
							icon="envelope"
							iconStyle={ComponentStyle.textInputIcon}
							placeholder={I18n.t('loginScreen.emailAddress')} />
					</View>
					<View style={LayoutStyle.row}>
						<TextInputValidate
							ref="password"
							style={ComponentStyle.textInputValidate}
							textInputStyle={textInputStyle}
							value={password}
							editable={editable}
							keyboardType="default"
							returnKeyType="go"
							autoCapitalize="none"
							autoCorrect={false}
							secureTextEntry
							onChangeText={(text) => this.setState({ password: text })}
							onSubmitEditing={this.handlePressLogin}
							errorTextStyle={ComponentStyle.errorText}
							errorInputStyle={ComponentStyle.textInputError}
							underlineColorAndroid="transparent"
							validators={this.validators.password}
							icon="lock"
							iconStyle={ComponentStyle.textInputIcon}
							placeholder={I18n.t('loginScreen.password')} />
					</View>
					<View style={LayoutStyle.inlineRow}>
						<Text style={LayoutStyle.rowLabel}>{I18n.t('loginScreen.rememberMe')}</Text>
						<Switch onValueChange={(bool:boolean) => this.setState({ remember: bool })}
							ref="remember"
							disabled={!editable}
							onTintColor={Colors.themeRed}
							value={remember} />
					</View>
					<View style={[LayoutStyle.row]}>
						<ActivityButton
							text={I18n.t('loginScreen.signIn')}
							animating={animating}
							indicatorStyle={ComponentStyle.buttonActivity}
							clickHandler={this.handlePressLogin}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
					<View style={[LayoutStyle.row]}>
						<Button text={I18n.t('loginScreen.signUp')}
							clickHandler={() => NavigationActions.signup()}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
					<View style={[LayoutStyle.row]}>
						<Button text={I18n.t('loginScreen.forgotPassword')}
							clickHandler={() => NavigationActions.forgotPassword({ email })}
							buttonStyle={[ComponentStyle.button, ComponentStyle.buttonActive]}
							buttonTextStyle={[ComponentStyle.buttonText, ComponentStyle.buttonTextActive]}
							buttonDownStyle={ComponentStyle.buttonDown}
							buttonDownTextStyle={ComponentStyle.buttonDownText} />
					</View>
				</View>
			</ScrollView>
		)
	}
}

const mapStateToProps = (state:Object, ownProps:Object) => {
	// get data from state and apply to props
	return {
		fetching: false,
		error: state.login.error,
		userData: state.login.user,
		savedLogin: state.savedLogin
	}
}

const mapDispatchToProps = (dispatch:Function) => {
	// map any dispatches to props
	return {
		attemptLogin: (email, password, remember) => dispatch(LoginActions.loginRequest(email, password, remember)),
		saveLogin: (email, password, remember) => dispatch(SavedLoginActions.saveLogin(email, password, remember))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
