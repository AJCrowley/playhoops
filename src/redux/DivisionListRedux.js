// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	divisionListRequest: ['eventId'],
	divisionListSuccess: ['result'],
	divisionListError: ['error']
	//divisionListClear: null
})

export const DivisionListTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	results: null,
	fetching: false
})

// on request set fetching to true
export const request = (state:Object) => state.merge({ fetching: true })

export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

export const success = (state:Object, result:Object) => {
	const { results } = result.result
	// merge state into redux store, fetching back to false
	return state.merge({ fetching: false, error: null, results })
}

//export const clear = (state) => INITIAL_STATE

export const reducer = createReducer(INITIAL_STATE, {
	[Types.DIVISION_LIST_REQUEST]: request,
	[Types.DIVISION_LIST_SUCCESS]: success,
	[Types.DIVISION_LIST_ERROR]: error
})
