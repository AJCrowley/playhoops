// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	signupRequest: ['firstName', 'lastName', 'email', 'password'],
	signupSuccess: ['result'],
	signupError: ['error']
})

export const SignupTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	fetching: false,
	signedUp: false
})

// on request set fetching to true
export const request = (state:Object) => state.merge({ fetching: true })

// for error just merge error message in with state
export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

// merge state into redux store, fetching back to false
export const success = (state:Object, result:Object) => state.merge({ fetching: false, error: null, signedUp: true })

export const reducer = createReducer(INITIAL_STATE, {
	[Types.SIGNUP_REQUEST]: request,
	[Types.SIGNUP_SUCCESS]: success,
	[Types.SIGNUP_ERROR]: error
})
