// @flow

import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../sagas/'

export default () => {
	const rootReducer = combineReducers({
		navigator: require('./NavigatorRedux').reducer,
		login: require('./LoginRedux').reducer,
		signup: require('./SignupRedux').reducer,
		savedLogin: require('./SavedLoginRedux').reducer,
		search: require('./SearchRedux').reducer,
		feedItem: require('./FeedItemRedux').reducer,
		events: require('./EventsRedux').reducer,
		bracket: require('./BracketRedux').reducer,
		teams: require('./TeamsRedux').reducer,
		divisionList: require('./DivisionListRedux').reducer,
		eventDetails: require('./EventDetailsRedux').reducer,
	})

	return configureStore(rootReducer, rootSaga)
}
