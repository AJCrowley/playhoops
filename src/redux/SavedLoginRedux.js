// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	saveLogin: ['email', 'password', 'remember'],
	getLogin: null,
	clearLogin: null
})

export const SavedLoginTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	email: null,
	password: null,
	remember: true
})

// merge email and password into state
export const save = (state:Object, result:Object) => state.merge({ email: result.email, password: result.password, remember: result.remember })

// reset state to initial
export const clear = (state:Object) => state.merge(INITIAL_STATE)

export const reducer = createReducer(INITIAL_STATE, {
	[Types.SAVE_LOGIN]: save,
	[Types.CLEAR_LOGIN]: clear
})
