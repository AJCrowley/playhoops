// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	bracketRequest: ['eventId', 'divisionId'],
	bracketSuccess: ['result'],
	bracketError: ['error']
})

export const BracketTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	results: null,
	fetching: false
})

// on request set fetching to true
export const request = (state:Object) => state.merge({ fetching: true })

// for error just merge error message in with state
export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

export const success = (state:Object, result:Object) => state.merge({ fetching: false, error: null, results: result.result })

export const reducer = createReducer(INITIAL_STATE, {
	[Types.BRACKET_REQUEST]: request,
	[Types.BRACKET_SUCCESS]: success,
	[Types.BRACKET_ERROR]: error
})
