// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	setHeaderImage: ['image'],
	clearHeaderImage: null,
	reactNativeRouterFluxFocus: null
})

export const NavigatorTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	headerImage: null,
	scene: null,
	homeTabHeaderImage: null,
	teamsTabHeaderImage: null
})

// merge image into state
export const setHeaderImage = (state:Object, action:Object) => state.merge({
	headerImage: action.image,
	homeTabHeaderImage: state.scene.parent == 'homeTab' ? action.image : state.homeTabHeaderImage,
	teamsTabHeaderImage: state.scene.parent == 'teamsTab' ? action.image : state.teamsTabHeaderImage,
})

// reset state to initial
export const clearHeaderImage = (state:Object) => state.merge({ headerImage: null })

// handle router navigation
export const reactNativeRouterFluxFocus = (state:Object, action:Object) => {
	const { scene } = action
	let headerImage
	switch(scene.parentTab) {
		case 'homeTab':
			headerImage = state.homeTabHeaderImage
			break
		case 'teamsTab':
			headerImage = state.teamsTabHeaderImage
			break
		default:
			headerImage = null
			break
	}
	return state.merge({ scene, headerImage })
}

export const reducer = createReducer(INITIAL_STATE, {
	[Types.SET_HEADER_IMAGE]: setHeaderImage,
	[Types.CLEAR_HEADER_IMAGE]: clearHeaderImage,
	[Types.REACT_NATIVE_ROUTER_FLUX_FOCUS]: reactNativeRouterFluxFocus
})
