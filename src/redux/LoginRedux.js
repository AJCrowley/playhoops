// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { Actions as NavigationActions } from 'react-native-router-flux'

const { Types, Creators } = createActions({
	loginRequest: ['email', 'password', 'remember'],
	loginSuccess: ['user'],
	loginError: ['error'],
	logout: null,
	loginReset: ['email'],
	loginResetSuccess: null,
	loginResetError: ['error']
})

export const LoginTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	fetching: false
})

// set fetching state to true
export const request = (state:Object) => state.merge({ fetching: true })

// for error just merge error message in with state
export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

// just set state back to initial (empty)
export const logout = (state:Object) => INITIAL_STATE

export const success = (state:Object, result:Object) => state.merge({ fetching: false, error: null, user: result.user.user })

export const loginReset = (state:Object) => state.merge({ fetching: true })

export const loginResetSuccess = (state:Object, result:Object) => state.merge({ fetching: false, error: null, reset: true })

export const loginResetError = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

export const reducer = createReducer(INITIAL_STATE, {
	[Types.LOGIN_REQUEST]: request,
	[Types.LOGIN_SUCCESS]: success,
	[Types.LOGIN_ERROR]: error,
	[Types.LOGOUT]: logout,
	[Types.LOGIN_RESET]: loginReset,
	[Types.LOGIN_RESET_SUCCESS]: loginResetSuccess,
	[Types.LOGIN_RESET_ERROR]: loginResetError
})
