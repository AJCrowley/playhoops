// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	eventsRequest: null,
	eventsSuccess: ['result'],
	eventsError: ['error']
})

export const EventsTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	results: null,
	fetching: false
})

// on request set fetching to true
export const request = (state:Object) => state.merge({ fetching: true })

// for error just merge error message in with state
export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

export const success = (state:Object, result:Object) => state.merge({ fetching: false, error: null, results: result.result })

export const reducer = createReducer(INITIAL_STATE, {
	[Types.EVENTS_REQUEST]: request,
	[Types.EVENTS_SUCCESS]: success,
	[Types.EVENTS_ERROR]: error
})
