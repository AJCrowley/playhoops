// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
	followRequest: ['itemType', 'id', 'followStatus'],
	followSuccess: ['result'],
	followError: ['error']
})

export const FeedItemTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
	error: null,
	results: null,
	fetching: false
})

// on request set fetching to true
export const request = (state:Object) => state.merge({ fetching: true })

// for error just merge error message in with state
export const error = (state:Object, { error }:Object) => state.merge({ fetching: false, error: error.error })

export const success = (state:Object, result:Object) => {
	//const { results } = result.result
	// merge state into redux store, fetching back to false
	return state.merge({ fetching: false, error: null })
}

export const reducer = createReducer(INITIAL_STATE, {
	[Types.FOLLOW_REQUEST]: request,
	[Types.FOLLOW_SUCCESS]: success,
	[Types.FOLLOW_ERROR]: error
})
