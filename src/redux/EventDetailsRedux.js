// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import I18n from 'react-native-i18n'

const { Types, Creators } = createActions({
	setEventDetailActiveTab: ['newActiveTab'],
	fetchStandingsRequest: ['eventId', 'divisionId'],
	fetchStandingsSuccess: ['standings'],
	fetchStandingsError: null,
	fetchGamesRequest: ['eventId', 'divisionId'],
	fetchGamesSuccess: ['gameList'],
	fetchGamesError: null
})

export const EventDetailsTypes = Types
export default Creators

/* NOTE: until dynamic generation of tab keys can be done from router structure, this must be added to
	manually when event tabs need to be added
*/
export const INITIAL_STATE = Immutable({
	activeTab: 0,
	standings: null,
	fetchingStandings: false,
	fetchingGames: false,
	gameList: null,
	gameListError: null,
	tabList: [
		{
			tabKey: 'eventScheduleWrapper',
			title: I18n.t('eventDetailTabs.scheduleTitle')
		},
		{
			tabKey: 'eventTeams',
			title: I18n.t('eventDetailTabs.teamsTitle')
		},
		{
			tabKey: 'eventStandings',
			title: I18n.t('eventDetailTabs.standingsTitle')
		},
		{
			tabKey: 'eventBracket',
			title: I18n.t('eventDetailTabs.bracketTitle')
		}
	]
})

// get standings
export const fetchStandingsRequest = (state:Object, action:Object) => state.merge({ fetchingStandings: true })

export const fetchStandingsSuccess = (state:Object, result:Object) =>  state.merge({ fetchingStandings: false, standings: result.standings.result })

export const fetchStandingsError = (state:Object, { error }:Object) => state.merge({ fetchingStandings: false, error: error.error })

// get event game list results
export const fetchGamesRequest = (state:Object, action:Object) => state.merge({ fetchingGames: true })

export const fetchGamesSuccess = (state:Object, { gameList }:Object) => (state.merge({ fetchingGames: false, gameList: gameList }))

export const fetchGamesError = (state:Object, { error }:Object) => state.merge({ fetchingGames: false, error: error.error })

//set active tab key when navigating through segmented tabs
export const setActiveTab = (state:Object, newActiveTabAction:Object) => {
	const { newActiveTab } = newActiveTabAction
	return state.merge({ activeTab: newActiveTab })
}

export const reducer = createReducer(INITIAL_STATE, {
	[Types.SET_EVENT_DETAIL_ACTIVE_TAB]: setActiveTab,
	[Types.FETCH_STANDINGS_REQUEST]: fetchStandingsRequest,
	[Types.FETCH_STANDINGS_SUCCESS]: fetchStandingsSuccess,
	[Types.FETCH_STANDINGS_ERROR]: fetchStandingsError,
	[Types.FETCH_GAMES_REQUEST]: fetchGamesRequest,
	[Types.FETCH_GAMES_SUCCESS]: fetchGamesSuccess,
	[Types.FETCH_GAMES_ERROR]: fetchGamesError
})
